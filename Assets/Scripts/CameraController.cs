namespace ForestFrenzy
{
    using ForestFrenzy.Managers;
    using UnityEngine;

    public class CameraController : MonoBehaviour
    {
        [SerializeField, Tooltip("This camera.")]
        private new Camera camera;
        public Camera Camera => camera;

        /// <summary>
        /// Is the player controlling the camera or nah?
        /// </summary>
        public bool PlayerControlsCamera { get; private set; }

        [SerializeField, Tooltip("The camera speed per frame.")]
        public Vector3 CameraSpeed;


        /// <summary>
        /// Checks if the player is controlling the camera. If not, then move the camera automatically.
        /// </summary>
        private void Update()
        {
            if (!Game.Instance.GameIsOver)
            {
                if (!PlayerControlsCamera)
                    transform.position += CameraSpeed * Time.deltaTime;
                else
                {
                    Vector3 newPos = new Vector3(PlayerController.instance.transform.position.x, transform.position.y, transform.position.z);
                    if (newPos.x < transform.position.x)
                        newPos.x = transform.position.x;

                    transform.position = newPos;
                }
            }
        }

        /// <summary>
        /// Sets the value if the player should control the camera or nah.
        /// </summary>
        /// <param name="playerControlsCamera"></param>
        public void SetPlayerControlsCamera(bool playerControlsCamera)
        {
            PlayerControlsCamera = playerControlsCamera;
        }
    }
}