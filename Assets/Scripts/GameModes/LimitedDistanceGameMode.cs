using ForestFrenzy;
using ForestFrenzy.GameModes;
using ForestFrenzy.Managers;
using UnityEngine;

[CreateAssetMenu(fileName = "New GameMode", menuName = "GameModes/Create Game Mode")]
internal class LimitedDistanceGameMode : GameMode
{
    [SerializeField, Tooltip("How far can the player go from the moment it starts?")]
    private int gameModeDistance = 120;

    private float gameModeStartDistance;

    [SerializeField, Tooltip("What's the minimum/maximum speed of the camera? This will change based on the game mode's duration.")]
    private Vector2 cameraHorizontalSpeedLimit;


    internal override void Start()
    {
        base.Start();

        gameModeStartDistance = 0;

        Fire.OnFireVisible -= StartTracker;
        Fire.OnFireVisible += StartTracker;
    }

    /// <summary>
    /// Checks if the player passed a certain co-ordinate. When they did, execute the next step.
    /// Also
    /// </summary>
    internal override void Update()
    {
        base.Update();

        if (Game.Instance.GameIsOver)
            return;

        // If the current timeline step is 1, then start this whole procedure.
        if (CurrentStep == 1)
        {
            if (!Game.Instance.CameraController.PlayerControlsCamera)
            {
                float currentGameModeDistance = PlayerController.instance.transform.position.x - gameModeStartDistance;
                float progress = currentGameModeDistance / gameModeDistance;

                Game.Instance.CameraController.CameraSpeed = Vector3.right * Mathf.Lerp(cameraHorizontalSpeedLimit.x, cameraHorizontalSpeedLimit.y, progress);
            }
        }

        // Win condition.
        CheckWinCondition();
    }

    /// <summary>
    /// Starts the tracker.
    /// </summary>
    internal void StartTracker()
    {
        gameModeStartDistance = PlayerController.instance.transform.position.x;

        Fire.OnFireVisible -= StartTracker;
    }

    /// <summary>
    /// Checks if the player won. (or lost... sadness)
    /// </summary>
    internal void CheckWinCondition()
    {
        if (gameModeStartDistance > 0 && PlayerController.instance.transform.position.x - gameModeStartDistance >= gameModeDistance)
            Win();
    }

    /// <summary>
    /// Disables the player controller and enables the end screen with some good news.
    /// </summary>
    internal override void Win()
    {
        // Pop-up end screen with good news and their score.
        base.Win();
    }

    /// <summary>
    /// Disables the player controller and enables the end screen with some bad news.
    /// </summary>
    internal override void Lose()
    {
        // Pop-up end screen with bad news and their score.
        base.Lose();
    }

    /// <summary>
    /// Disables the pause screen and then resumes the game.
    /// </summary>
    internal override void Resume()
    {
        base.Resume();

        IsPaused = false;
    }

    /// <summary>
    /// Enables the pause screen and then resumes the game.
    /// </summary>
    internal override void Pause()
    {
        base.Pause();

        IsPaused = true;
    }
}
