namespace ForestFrenzy.GameModes
{
    using ForestFrenzy.Managers;
    using UnityEngine;
    using UnityEngine.Events;

    internal abstract class GameMode : ScriptableObject
    {
        protected bool IsPaused { get; set; }
        protected int CurrentStep { get; set; }
        public UnityEvent WonGame;
        public UnityEvent LoseGame;


        /// <summary>
        /// Starts the game mode itself and reset variables.
        /// </summary>
        internal virtual void Start()
        {
            CurrentStep = 0;
            IsPaused = false;
            WonGame.AddListener(Win);
            LoseGame.AddListener(Lose);
        }

        internal virtual void ChangeStep(int stepNo)
        {
            CurrentStep = stepNo;
        }

        /// <summary>
        /// Updates the game mode.
        /// </summary>
        internal virtual void Update()
        {

        }

        internal virtual void Win()
        {
            Debug.Log("Big winner.");
            Game.Instance.GameOver(true);
        }

        internal virtual void Lose()
        {
            Debug.Log("Big loser.");
            Game.Instance.GameOver(false);
        }

        internal virtual void Pause()
        {

        }

        internal virtual void Resume()
        {

        }
    }
}
