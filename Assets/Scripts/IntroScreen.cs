using ForestFrenzy.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class IntroScreen : MonoBehaviour
{
    [SerializeField]
    private InputActionAsset inputFile;

    [SerializeField]
    private Animator animator;

    private bool gameStarted = false;

    private void Update()
    {
        if (gameStarted) return;

        if (inputFile.FindAction("Start").WasPressedThisFrame())
        {
            Game.Instance.StartGameMode(0);
            animator.SetTrigger("Start");
            gameStarted = true;
        }
    }
}
