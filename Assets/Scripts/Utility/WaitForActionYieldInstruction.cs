﻿namespace ForestFrenzy.Utilities
{
    using UnityEngine;

    public class WaitForActionYieldInstruction : CustomYieldInstruction
    {
        /// <summary>
        /// If this boolean is false, the coroutine will wait until it can proceed. If it’s true (e.g. after a successful action) the co-routine will continue, but also immediately make it false again.
        /// </summary>
        private bool canContinueCoroutine = false;

        public override bool keepWaiting
        {
            get
            {
                bool canContinue = canContinueCoroutine;
                if (canContinueCoroutine)
                    canContinueCoroutine = false;

                return !canContinue;
            }
        }

        /// <summary>
        /// Resets the yield instruction and sets canContinueCoroutine false.
        /// </summary>
        public override void Reset()
        {
            base.Reset();
            canContinueCoroutine = false;
        }

        /// <summary>
        /// Continues the coroutine.
        /// </summary>
        public void ContinueCoroutine()
        {
            canContinueCoroutine = true;
        }
    }
}
