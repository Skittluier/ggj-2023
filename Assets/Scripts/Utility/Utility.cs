﻿using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace ForestFrenzy.Utilities
{
    public static class Utility
    {
#if UNITY_EDITOR
        public static void DrawDebugGizmo(Vector3 position, Quaternion rotation, float size = 0.15f)
        {
            Debug.DrawLine(position, position + rotation * Vector3.up * size, Handles.xAxisColor);
            Debug.DrawLine(position, position + rotation * Vector3.right * size, Handles.yAxisColor);
            Debug.DrawLine(position, position + rotation * Vector3.forward * size, Handles.zAxisColor);
        }

#endif
        /// <summary>
        /// Determines if given gameObject has its layer in layermask
        /// </summary>
        /// <returns>True if gameObjects layer is stored in given layerMask</returns>
        public static bool IsInLayerMask(this GameObject go, LayerMask layermask)
        {
            return layermask == (layermask.value | (1 << go.layer));
        }


        /// <summary>
        /// Projects point p onto rotated bounds so that p never leaves the rotated bounds
        /// </summary>
        /// <param name="bounds">The original axis alligned bounds</param>
        /// <param name="p">The point to project</param>
        /// <param name="q">The rotation of the bounds</param>
        /// <returns>Point p so that it always lies inside or on the edge of the rotated bounds</returns>
        public static Vector3 RotatedProject(this Bounds bounds, Vector3 p, Quaternion q)
        {
            Vector3 c = bounds.center;
            Vector3 f = q * Vector3.forward;
            Vector3 r = q * Vector3.right;
            Vector3 u = q * Vector3.up;

            float x = Mathf.Clamp(Utility.SignedDistanceFromPlane(p, r, c), -bounds.extents.x, bounds.extents.x);
            float y = Mathf.Clamp(Utility.SignedDistanceFromPlane(p, u, c), -bounds.extents.y, bounds.extents.y);
            float z = Mathf.Clamp(Utility.SignedDistanceFromPlane(p, f, c), -bounds.extents.z, bounds.extents.z);

            return c + (f * z) + (r * x) + (y * u);
        }

        /// <summary>
        /// Checks if given point is inside rotated AABB bounds
        /// </summary>
        /// <param name="bounds">The original axis alligned bounds</param>
        /// <param name="p">The point to check</param>
        /// <param name="q">The rotation of the bounds</param>
        /// <returns>True if point lies in rotated bounds</returns>
        public static bool RotatedContains(this Bounds bounds, Vector3 p, Quaternion q)
        {
            Vector3 c = bounds.center;
            Vector3 f = q * Vector3.forward;
            Vector3 r = q * Vector3.right;
            Vector3 u = q * Vector3.up;

            float x = Utility.SignedDistanceFromPlane(p, r, c);
            float y = Utility.SignedDistanceFromPlane(p, u, c);
            float z = Utility.SignedDistanceFromPlane(p, f, c);

            return (x > -bounds.extents.x && x < bounds.extents.x) && (y > -bounds.extents.y && y < bounds.extents.y) && (z > -bounds.extents.z && z < bounds.extents.z);
        }

        /// <summary>
        /// Returns the average of all contact points in given collision
        /// </summary>
        public static Vector3 GetContactPointAverage(this Collision collision)
        {
            Vector3 avg = Vector3.zero;
            for (int i = 0; i < collision.contactCount; i++)
                avg += collision.GetContact(i).point;

            return avg /= collision.contactCount;
        }

        /// <summary>
        /// Remaps given value from range between: oldMin-oldMax to range of newMin-newMax
        /// </summary>
        /// <param name="value">The value to remap</param>
        /// <param name="oldMin">The old minimum</param>
        /// <param name="oldMax">The old maximum</param>
        /// <param name="newMin">The new minumum</param>
        /// <param name="newMax">The new maximum</param>
        public static float Remap(this float value, float oldMin, float oldMax, float newMin, float newMax)
        {
            return (value - oldMin) / (oldMax - oldMin) * (newMax - newMin) + newMin;
        }

        /// <summary>
        /// Returns given amount of frames in time, reference speed is taken from sampleFPS
        /// </summary>
        public static float FramesToTime(this int fps, int sampleFPS = 90)
        {
            return Utility.Remap(fps, 0, sampleFPS, 0, 1);
        }

        /// <summary>
        /// Recursive function for finding a child transform by name
        /// </summary>
        public static Transform FindChildRecursive(this Transform parent, string childName)
        {
            Queue<Transform> queue = new Queue<Transform>();
            queue.Enqueue(parent);
            while (queue.Count > 0)
            {
                var c = queue.Dequeue();
                if (c.name == childName)
                    return c;
                foreach (Transform t in c)
                    queue.Enqueue(t);
            }
            return null;
        }



        #region Matrix

        public static Quaternion GetRotation(this Matrix4x4 matrix)
        {
            Quaternion q = new Quaternion();
            q.w = Mathf.Sqrt(Mathf.Max(0, 1 + matrix.m00 + matrix.m11 + matrix.m22)) / 2;
            q.x = Mathf.Sqrt(Mathf.Max(0, 1 + matrix.m00 - matrix.m11 - matrix.m22)) / 2;
            q.y = Mathf.Sqrt(Mathf.Max(0, 1 - matrix.m00 + matrix.m11 - matrix.m22)) / 2;
            q.z = Mathf.Sqrt(Mathf.Max(0, 1 - matrix.m00 - matrix.m11 + matrix.m22)) / 2;
            q.x = _copysign(q.x, matrix.m21 - matrix.m12);
            q.y = _copysign(q.y, matrix.m02 - matrix.m20);
            q.z = _copysign(q.z, matrix.m10 - matrix.m01);
            return q;
        }

        private static float _copysign(float sizeval, float signval)
        {
            return Mathf.Sign(signval) == 1 ? Mathf.Abs(sizeval) : -Mathf.Abs(sizeval);
        }

        public static Vector3 GetPosition(this Matrix4x4 matrix)
        {
            var x = matrix.m03;
            var y = matrix.m13;
            var z = matrix.m23;

            return new Vector3(x, y, z);
        }

        #endregion

        /// <summary>
        /// Increments given index so it cycles over 0-max
        /// </summary>
        /// <param name="index">Index to increment</param>
        /// <param name="incrementation">Incrementation amount</param>
        /// <param name="max">Max value for index</param>
        public static int CycleIndex(int index, int incrementation, int max)
        {
            index += incrementation;

            if (index >= max)
                index = index % max;
            if (index < 0)
                index = max - Mathf.Abs(index);

            return index;
        }

        /// <summary>
        /// Removes the (Clone) at the end of spawned objects (if found)
        /// </summary>
        public static string RemoveCloneFromString(this string s)
        {
            if (s.Length == 0)
                return s;

            //Very naive search lmao
            if (s[s.Length - 1] != ')')
                return s;
            else
                return s.Substring(0, s.Length - 7);
        }

        /// <summary>
        /// Returns a random number between 2 given vector components
        /// </summary>
        public static float RandomInRange(this Vector2 minMax)
        {
            return Random.Range(minMax.x, minMax.y);
        }

        /// <summary>
        /// Rotates point around a pivot point.
        /// </summary>
        /// <param name="point">Point to rotate</param>
        /// <param name="pivot">Pivot point to rotate around</param>
        /// <param name="angles">Amount of angles to rotate</param>
        public static Vector3 RotatePointAroundPivot(this Vector3 point, Vector3 pivot, Vector3 angles)
        {
            return Quaternion.Euler(angles) * (point - pivot) + pivot;
        }

        /// <summary>
        /// Projects given point onto given ray
        /// </summary>
        /// <param name="rayStart">Start point of the ray</param>
        /// <param name="rayEnd">End point of the ray</param>
        /// <param name="point">The point to project onto the ray</param>
        public static Vector3 ProjectPointOnRay(Vector3 rayStart, Vector3 rayEnd, Vector3 point)
        {
            Vector3 ap = point - rayStart;
            Vector3 dir = rayEnd - rayStart;

            return rayStart + Vector3.Dot(ap, dir) / Vector3.Dot(dir, dir) * dir;
        }

        /// <summary>
        /// Determines if given point is left of given ray
        /// </summary>
        /// <param name="rayStart">Start point of ray</param>
        /// <param name="rayEnd">End point of ray</param>
        /// <param name="point">The point to check</param>
        public static bool IsLeftOfRay(Vector2 rayStart, Vector2 rayEnd, Vector2 point)
        {
            return ((rayEnd.x - rayStart.x) * (point.y - rayStart.y) - (rayEnd.y - rayStart.y) * (point.x - rayStart.x)) > 0;
        }

        /// <summary>
        /// Returns the signed distance between given point and surface of given plane
        /// </summary>
        /// <param name="point">Point to check distance for</param>
        /// <param name="planeNormal">Normal vector of plane</param>
        /// <param name="planePoint">origin point of plane</param>
        public static float SignedDistanceFromPlane(this Vector3 point, Vector3 planeNormal, Vector3 planePoint)
        {
            return Vector3.Dot(planeNormal, point - planePoint);
        }

        /// <summary>
        /// Clamps given angle so it never extends max or gets below min
        /// </summary>
        /// <param name="angle">The angle to clamp</param>
        /// <param name="min">The min value of the angle</param>
        /// <param name="max">The max value of the angle</param>
        /// <param name="lastClampedMax">The last clamped max value</param>
        public static float ClampAngle(float angle, float min, float max, float lastClampedMax = 0f)
        {
            //TODO: Profile this function, this might be queit some operations for just clamping an angle.
            float normalizedMin = NormalizeAngle(min);
            float normalizedMax = NormalizeAngle(max);
            float normalizedAngle = NormalizeAngle(angle);

            if ((normalizedMin == 0 || normalizedMax == 360) && (normalizedMin == 360 && normalizedMax == 0))
                return normalizedAngle;

            bool clipToAngleEdge = false;
            bool negativeAngles = (min < 0 || max < 0);

            if (negativeAngles)
            {
                if (min < 0 && max < 0)
                {
                    if (Mathf.Clamp(normalizedAngle, normalizedMin, normalizedMax) != normalizedAngle)
                        clipToAngleEdge = true;
                }
                else if (min < 0 && normalizedAngle > normalizedMin && normalizedAngle <= 360)
                    clipToAngleEdge = false;
                else if (max < 0 && normalizedAngle > normalizedMax && normalizedAngle <= 360)
                    clipToAngleEdge = false;
                else
                {
                    float testAngle = angle % 360;

                    if (Mathf.Clamp(testAngle, min, max) != testAngle)
                        clipToAngleEdge = true;
                }
            }
            else
            {
                if ((normalizedAngle < Mathf.Min(normalizedMin, normalizedMax)) || (normalizedAngle > Mathf.Max(normalizedMin, normalizedMax)))
                    clipToAngleEdge = true;
            }

            if (clipToAngleEdge)
            {
                float minDelta = Mathf.Abs(Mathf.DeltaAngle(angle, min));
                float maxDelta = Mathf.Abs(Mathf.DeltaAngle(angle, max));

                if (lastClampedMax == minDelta)
                    return minDelta;

                if (lastClampedMax == maxDelta)
                    return maxDelta;

                return (minDelta < maxDelta) ? min : max;
            }

            return angle;
        }

        /// <summary>
        /// Normalizes angle so its always between 0 - 360 degrees
        /// </summary>
        public static float NormalizeAngle(float angle)
        {
            //Dirty, we should not be doing this, but otherwise the modulo operator will make 360 degrees into 0 degrees
            if (angle == 360)
                return angle;

            angle = angle %= Mathf.Sign(angle) * 360;

            if (angle < 0)
                angle = 360 + angle;

            return angle;
        }

        /// <summary>
        /// Normalizes given value between the range of: start and end
        /// </summary>
        /// <param name="value">The value to normalize</param>
        /// <param name="start">The starting value of the range</param>
        /// <param name="end">The ending value of the range</param>
        public static float Normalize(this float value, float start, float end)
        {
            float range = end - start;
            float offset = value - start;

            return (offset - (Mathf.Floor(offset / range) * range)) + start;
        }

        /// <summary>
        /// Returns the signed angle between of: normal vector
        /// </summary>
        /// <param name="referenceVector">First reference vector for calculating angle</param>
        /// <param name="otherVector">Second reference vector for calculating angle</param>
        /// <param name="normal">The direction that determines the angle</param>
        public static float SignedVectorAngle(Vector3 referenceVector, Vector3 otherVector, Vector3 normal)
        {
            Vector3 perpVector;
            float angle;

            //Use the geometry object normal and one of the input vectors to calculate the perpendicular vector
            perpVector = Vector3.Cross(normal, referenceVector);

            //Now calculate the dot product between the perpendicular vector (perpVector) and the other input vector
            angle = Vector3.Angle(referenceVector, otherVector);
            angle *= Mathf.Sign(Vector3.Dot(perpVector, otherVector));

            if (angle < 0)
                angle = 180 + (180 + angle);

            return angle;
        }

        /// <summary>
        /// Clamps given point to line segment
        /// </summary>
        /// <param name="lineStart">Start point of the line segment</param>
        /// <param name="lineEnd">End point of the line segment</param>
        /// <param name="point">The point to clamp onto the line segment</param>
        public static Vector3 ClampPointOnLine(Vector3 lineStart, Vector3 lineEnd, Vector3 point)
        {
            Vector3 center = (lineEnd + lineStart) * 0.5f;
            Vector3 up = (lineEnd - center).normalized;

            float currentDistance = point.SignedDistanceFromPlane(up, center);
            float maxDistance = lineEnd.SignedDistanceFromPlane(up, center);

            if (currentDistance >= maxDistance)
                return center + (up.normalized * maxDistance);
            if (currentDistance <= -maxDistance)
                return center + (up.normalized * -maxDistance);

            return point;
        }

        /// <summary>
        /// Makes a 1x1 quad
        /// </summary>
        public static Mesh CreateQuad()
        {
            Mesh quad = new Mesh
            {
                vertices = new Vector3[]
                {
                    new Vector3(-1,-1,0),
                    new Vector3(-1,1,0),
                    new Vector3(1,1,0),
                    new Vector3(1,-1,0),
                },

                normals = new Vector3[]
                {
                    Vector3.up,
                    Vector3.up,
                    Vector3.up,
                    Vector3.up
                },

                tangents = new Vector4[]
                {
                    Vector3.forward,
                    Vector3.forward,
                    Vector3.forward,
                    Vector3.forward
                },

                triangles = new int[]
                {
                    2,1,0,
                    0,3,2
                },

                uv = new Vector2[]
                {
                    new Vector2(0,0),
                    new Vector2(0,1),
                    new Vector2(1,1),
                    new Vector2(1,0),
                }
            };

            return quad;
        }

        /// <summary>
        /// Sets layer for given gameObject and all children recursively
        /// </summary>
        public static void SetLayerRecursively(this GameObject go, int layer)
        {
            go.layer = layer;

            int childCount = go.transform.childCount;

            for (int i = 0; i < childCount; i++)
                SetLayerRecursively(go.transform.GetChild(i).gameObject, layer);
        }

        /// <summary>
        /// Draws a beatifull gizmo for you
        /// </summary>
        public static void DrawGizmo(Vector3 pos, Quaternion rot, Color col, float size)
        {
            Vector3 top = pos + Vector3.up * size;
            Vector3 bot = pos - Vector3.up * size;
            Vector3 right = pos + Vector3.right * size;
            Vector3 left = pos - Vector3.right * size;
            Vector3 forward = pos + Vector3.forward * size;
            Vector3 back = pos - Vector3.forward * size;

            top = RotatePointAroundPivot(top, pos, rot.eulerAngles);
            bot = RotatePointAroundPivot(bot, pos, rot.eulerAngles);
            left = RotatePointAroundPivot(left, pos, rot.eulerAngles);
            right = RotatePointAroundPivot(right, pos, rot.eulerAngles);
            forward = RotatePointAroundPivot(forward, pos, rot.eulerAngles);
            back = RotatePointAroundPivot(back, pos, rot.eulerAngles);

            Debug.DrawLine(top, bot, col);
            Debug.DrawLine(left, right, col);
            Debug.DrawLine(back, forward, col);
        }

        /// <summary>
        /// Returns position on bounds in world space.
        /// </summary>  
        /// <param name="mf">The meshFilter to get the bounds from</param>
        /// <param name="point">Relative point on the bounds to get in world space</param>
        public static Vector3 GetPositionOnBounds(this MeshFilter mf, BoundsPoint point)
        {
            if (mf == null) return mf.transform.position;

            Bounds b = mf.mesh.bounds;
            Matrix4x4 m = mf.transform.localToWorldMatrix;

            switch (point)
            {
                default:
                case BoundsPoint.FrontMiddle:
                    return m.MultiplyPoint(b.center + new Vector3(0, 0, b.extents.z));
                case BoundsPoint.BackMiddle:
                    return m.MultiplyPoint(b.center + new Vector3(0, 0, -b.extents.z));
                case BoundsPoint.RightMiddle:
                    return m.MultiplyPoint(b.center + new Vector3(b.extents.x, 0, 0));
                case BoundsPoint.LeftMiddle:
                    return m.MultiplyPoint(b.center + new Vector3(-b.extents.x, 0, 0));
                case BoundsPoint.TopMiddle:
                    return m.MultiplyPoint(b.center + new Vector3(0, b.extents.y, 0));
                case BoundsPoint.BotMiddle:
                    return m.MultiplyPoint(b.center + new Vector3(0, -b.extents.y, 0));
                case BoundsPoint.Center:
                    return m.MultiplyPoint(b.center);
            }
        }

        /// <summary>
        /// Returns position on AABB bounds in world space
        /// </summary>
        /// <param name="mr"></param>
        /// <param name="point"></param>
        /// <returns></returns>
        public static Vector3 GetPositionOnBoundsAABB(this Renderer mr, BoundsPoint point)
        {
            if (mr == null) return mr.transform.position;
            Bounds b = mr.bounds;

            switch (point)
            {
                default:
                case BoundsPoint.FrontMiddle:
                    return b.center + new Vector3(0, 0, b.extents.z);
                case BoundsPoint.BackMiddle:
                    return b.center + new Vector3(0, 0, -b.extents.z);
                case BoundsPoint.RightMiddle:
                    return b.center + new Vector3(b.extents.x, 0, 0);
                case BoundsPoint.LeftMiddle:
                    return b.center + new Vector3(-b.extents.x, 0, 0);
                case BoundsPoint.TopMiddle:
                    return b.center + new Vector3(0, b.extents.y, 0);
                case BoundsPoint.BotMiddle:
                    return b.center + new Vector3(0, -b.extents.y, 0);
                case BoundsPoint.Center:
                    return b.center;
            }
        }

        //Determines a certain position on bounds
        public enum BoundsPoint
        {
            TopMiddle,
            FrontMiddle,
            BackMiddle,
            RightMiddle,
            LeftMiddle,
            BotMiddle,
            Center
        }
    }
}
