using UnityEngine;
using System.Collections;

public class CameraShake : MonoBehaviour
{
	// Transform of the camera to shake. Grabs the gameObject's transform
	// if null.
	public Transform camTransform;

	// How long the object should shake for.
	public float shakeDuration = 0f;

	// Amplitude of the shake. A larger value shakes the camera harder.
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;
	public float rotateAmount = 0f;
	public float cameraSizeAmount = 0;
	public AudioManager audioManager;

	Vector3 originalPos;
	private float orthographicSize;

	void Awake()
	{
		orthographicSize = Camera.main.orthographicSize;

		if (camTransform == null)
		{
			camTransform = GetComponent<Transform>();
		}
	}

	void OnEnable()
	{
		originalPos = camTransform.localPosition;
	}

	void Update()
	{
		
		if (shakeDuration > 0)
		{
			var shakeAmplitude = shakeAmount * audioManager.amplitude;

			camTransform.localEulerAngles = new Vector3(0f, 0f, shakeAmplitude * rotateAmount * Random.Range(-1f, 1f));
			camTransform.localPosition = originalPos + Random.insideUnitSphere * shakeAmplitude;
			Camera.main.orthographicSize = orthographicSize + cameraSizeAmount * Random.Range(-1f, 1f) * shakeAmplitude;

			shakeDuration -= Time.deltaTime * decreaseFactor;
		}
		else
		{
			shakeDuration = 0f;

			if (audioManager.amplitude > audioManager.amplitudeThreshold) {

				shakeDuration = 0.5f;
            }

			camTransform.localPosition = originalPos;
		}
	}
}