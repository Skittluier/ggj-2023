﻿using System;
using System.Collections.Generic;

namespace UnityEngine.Events
{
    /// <summary>
    /// Unity Event with bool as parameter
    /// </summary>
    [Serializable]
    public class UnityEventBool : UnityEvent<bool> { }

    /// <summary>
    /// Unity Event with int as parameter
    /// </summary>
    [Serializable]
    public class UnityEventInt : UnityEvent<int> { }

    /// <summary>
    /// Unity Event with color as parameter
    /// </summary>
    [Serializable]
    public class UnityEventColor : UnityEvent<Color> { }

    /// <summary>
    /// Unity Event with float as parameter
    /// </summary>
    [Serializable]
    public class UnityEventFloat : UnityEvent<float> { }

    /// <summary>
    /// Unity Event with collider as parameter
    /// </summary>
    [Serializable]
    public class UnityEventCollider : UnityEvent<Collider2D> { }

    /// <summary>
    /// Unity Event with collision as parameter
    /// </summary>
    [Serializable]
    public class UnityEventCollision : UnityEvent<Collision2D> { }

    /// <summary>
    /// Unity event with string as parameter
    /// </summary>
    [Serializable]
    public class StringEvent : UnityEvent<string> { }

    /// <summary>
    /// Unity event with short and string as parameter
    /// </summary>
    [Serializable]
    public class ShortStringEvent : UnityEvent<short, string> { }

    /// <summary>
    /// Unity event with Dictionary[string, object] as parameter. 
    /// </summary>
    public class DictionaryStringObjectEvent : UnityEvent<Dictionary<string, object>> { }

    /// <summary>
    /// Unity event with string, string array and object array parameters.
    /// </summary>
    public sealed class StringStringArrayObjectArrayEvent : UnityEvent<string, string[], object[]> { }

    /// <summary>
    /// Unity event with string, object, string parameters.
    /// </summary>
    public sealed class StringObjectStringEvent : UnityEvent<string, object, string> { }

    /// <summary>
    /// Unity event with string, int, bool, object parameters.
    /// </summary>
    public sealed class StringIntBoolObjectEvent : UnityEvent<string, int, bool, object> { }

    /// <summary>
    /// Unity event with string array and bool array parameters.
    /// </summary>
    public sealed class StringArrayBoolArrayEvent : UnityEvent<string[], bool[]> { }

    /// <summary>
    /// Unity event with string array parameters.
    /// </summary>
    public sealed class StringArrayEvent : UnityEvent<string[]> { }

    /// <summary>
    /// Unity event with string parameters.
    /// </summary>
    public sealed class StringStringEvent : UnityEvent<string, string> { }
}
