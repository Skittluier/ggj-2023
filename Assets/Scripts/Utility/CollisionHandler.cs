﻿using ForestFrenzy.Utilities;
using NaughtyAttributes;
using UnityEngine;
using UnityEngine.Events;

namespace ForestFrenzy.Interactions
{
    public class CollisionHandler : MonoBehaviour
    {
        public Collider2D ThisCollider
        {
            get;
            private set;
        }

        [SerializeField]
        private bool triggerOnce, useSpecificLayerMask, useDelay;

        private bool onceTriggerEntered, onceTriggerExited;

        [SerializeField, ShowIf("useSpecificLayerMask")]
        private LayerMask respondingLayerMask;

        [SerializeField, ShowIf("useDelay")]
        private float actionDelay;

        [SerializeField]
        private UnityEventCollider onTriggerEnter, onTriggerExit;

        [SerializeField]
        private UnityEventCollision onCollisionEnter, onCollisionExit;

        public UnityEventCollider TriggerEnter => onTriggerEnter;

        public UnityEventCollider TriggerStay
        {
            get;
            set;
        } = new UnityEventCollider();

        public UnityEventCollider TriggerExit => onTriggerExit;

        public UnityEventCollision CollisionEnter => onCollisionEnter;

        public UnityEventCollision CollisionStay
        {
            get;
            set;
        } = new UnityEventCollision();

        public UnityEventCollision CollisionExit => onCollisionExit;


#if UNITY_EDITOR
        [Button("Execute OnTriggerEnter/OnTriggerExit/OnCollisionEnter", EButtonEnableMode.Playmode)]
        private void ExecuteActions()
        {
            onTriggerEnter.Invoke(null);
            onTriggerExit.Invoke(null);
            onCollisionEnter.Invoke(null);
        }
#endif

        private void Awake()
        {
            ThisCollider = GetComponent<Collider2D>();
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (!enabled || (triggerOnce && onceTriggerEntered) || (useSpecificLayerMask && !other.gameObject.IsInLayerMask(respondingLayerMask)))
                return;

            TriggerEnter?.Invoke(other);

            if (triggerOnce)
                onceTriggerEntered = true;
        }
        private void OnTriggerStay2D(Collider2D other)
        {
            if (!enabled || (triggerOnce && onceTriggerExited) || (useSpecificLayerMask && !other.gameObject.IsInLayerMask(respondingLayerMask)))
                return;

            TriggerStay?.Invoke(other);
        }
        private void OnTriggerExit2D(Collider2D other)
        {
            if (!enabled || (triggerOnce && onceTriggerExited) || (useSpecificLayerMask && !other.gameObject.IsInLayerMask(respondingLayerMask)))
                return;

            TriggerExit?.Invoke(other);

            if (triggerOnce)
                onceTriggerExited = true;
        }
        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (!enabled || (triggerOnce && onceTriggerEntered) || (useSpecificLayerMask && !collision.transform.gameObject.IsInLayerMask(respondingLayerMask)))
                return;

            CollisionEnter?.Invoke(collision);

            if (triggerOnce)
                onceTriggerEntered = true;
        }
        private void OnCollisionStay2D(Collision2D collision)
        {
            if (!enabled || (triggerOnce && onceTriggerExited) || (useSpecificLayerMask && !collision.transform.gameObject.IsInLayerMask(respondingLayerMask)))
                return;

            CollisionStay?.Invoke(collision);
        }
        private void OnCollisionExit2D(Collision2D collision)
        {
            if (!enabled || (triggerOnce && onceTriggerExited) || (useSpecificLayerMask && !collision.transform.gameObject.IsInLayerMask(respondingLayerMask)))
                return;

            CollisionExit?.Invoke(collision);

            if (triggerOnce)
                onceTriggerExited = true;
        }
    }
}
