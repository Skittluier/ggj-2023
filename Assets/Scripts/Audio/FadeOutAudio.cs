namespace ForestFrenzy.Audio
{
    using System.Collections;
    using UnityEngine;

    public class FadeOutAudio : MonoBehaviour
    {
        [SerializeField]
        private AudioSource targetAudioSource;

        [SerializeField]
        private AnimationCurve fadeOutCurve;

        [SerializeField]
        private float fadeOutDuration;


        private IEnumerator Start()
        {
            float currVal = 0;
            float fromVolume = targetAudioSource.volume;

            while (currVal < 1)
            {
                currVal += Time.deltaTime / fadeOutDuration;
                targetAudioSource.volume = Mathf.Lerp(fromVolume, 0, fadeOutCurve.Evaluate(currVal));

                yield return null;
            }

            targetAudioSource.volume = 0;
        }
    }
}
