namespace ForestFrenzy.Managers
{
    using ForestFrenzy.GameModes;
    using NaughtyAttributes;
    using RUMBLE.Utilities;
    using System;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.SceneManagement;

    public class Game : Singleton<Game>
    {
        [Serializable]
        internal struct GameModeReference
        {
            [SerializeField, Tooltip("The current game mode in the game.")]
            internal GameMode currentGameMode;

            [SerializeField, Tooltip("A game object that will be permanently enabled whenever this game mode is active.")]
            internal GameObject permanentGameObject;

            [SerializeField, Tooltip("Game objects that belong to the steps for the game mode.")]
            internal GameObject[] stepGOs;
        }

        [SerializeField, Tooltip("The game modes.")]
        private GameModeReference[] gameModes;
        private int activeGameModeNo = -1;

        [SerializeField, Tooltip("The game camera controller.")]
        private CameraController cameraController;
        public CameraController CameraController => cameraController;

        [SerializeField, Tooltip("How many lives are here until the players lost? At least 1 player is required.")]
        private int lifeAmount = 0;
        public bool GameIsOver { get; private set; }

        [SerializeField]
        private Menu menu;

        [SerializeField]
        private ParticleSystem fireParticle;




        protected override void Awake()
        {
            base.Awake();

            for (int i = 0; i < gameModes.Length; i++)
            {
                gameModes[i].permanentGameObject.SetActive(false);

                for (int j = 0; j < gameModes[i].stepGOs.Length; j++)
                    gameModes[i].stepGOs[j].SetActive(false);
            }
        }

        /// <summary>
        /// Updates the game mode.
        /// </summary>
        private void Update()
        {
            if (activeGameModeNo != -1 && !GameIsOver)
            {
                gameModes[activeGameModeNo].currentGameMode.Update();

                if (lifeAmount <= 0)
                    gameModes[activeGameModeNo].currentGameMode.Lose();
            }
        }

#if UNITY_EDITOR
        [Button]
        private void EDITOR_StartFirstGameMode()
        {
            StartGameMode(0);
        }
#endif

        /// <summary>
        /// Activates the game object belonging to this gamemode and executes the gamemode's Start method.
        /// </summary>
        public void StartGameMode(int gameModeNo)
        {
            GameModeReference gm = gameModes[gameModeNo];

            gm.permanentGameObject.SetActive(true);
            gm.currentGameMode.Start();

            activeGameModeNo = gameModeNo;
            ExecuteGameModeStep(0);
        }


        /// <summary>
        /// Restarts the game.
        /// </summary>
        public void Restart()
        {
            SceneManager.LoadScene(0);
        }

        /// <summary>
        /// If there is a next step, go to the next step of the game mode.
        /// </summary>
        public void ExecuteGameModeStep(int stepNo)
        {
            if (activeGameModeNo == -1)
            {
                Debug.LogError("[Game] No active game mode!");
                return;
            }

            gameModes[activeGameModeNo].currentGameMode.ChangeStep(stepNo);
            gameModes[activeGameModeNo].stepGOs[stepNo].SetActive(true);
        }

        /// <summary>
        /// Game over.
        /// </summary>
        public void GameOver(bool bigWinner)
        {
            GameIsOver = true;

            if (bigWinner)
            {
                fireParticle.Stop(true, ParticleSystemStopBehavior.StopEmitting);
                menu.ShowWinScreen();
            }
            else
                menu.ShowLoseScreen();
        }

        /// <summary>
        /// Will increase the life amount with the amount of tentacles that join.
        /// </summary>
        public void OnPlayedJoined()
        {
            if (!GameIsOver)
                lifeAmount += 2;
        }

        /// <summary>
        /// Retracts one life.
        /// </summary>
        public void RetractLife()
        {
            lifeAmount--;

            if (lifeAmount <= 0)
                gameModes[activeGameModeNo].currentGameMode.Lose();
        }
    }
}