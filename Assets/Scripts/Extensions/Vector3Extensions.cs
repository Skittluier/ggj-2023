﻿using UnityEngine;
public static class Vector3Extensions
{
    public static Vector3 WithX (this Vector3 vector, float val)
    {
        return new Vector3(val, vector.y, vector.z);
    }

    public static Vector3 WithY(this Vector3 vector, float val)
    {
        return new Vector3(vector.x, val, vector.z);
    }

    public static Vector3 WithZ(this Vector3 vector, float val)
    {
        return new Vector3(vector.x, vector.y, val);
    }
}
