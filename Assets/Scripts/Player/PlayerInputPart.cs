using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputPart : MonoBehaviour
{
    [SerializeField, Tooltip("The left and right tentacles")]
    internal PhysicsTentacle leftTentacle, rightTentacle;

    [SerializeField, Tooltip("The input component")]
    internal PlayerInput input;

    [SerializeField, Tooltip("array of test player colors")]
    internal Color[] testPlayerColors;

    /// <summary>
    /// Can this player be controlled?
    /// </summary>
    public static bool CanBeControlled;

    /// <summary>
    /// Called on the first active frame, attaches to the player instance 
    /// </summary>
    private void Start()
    {
        int playerIndex = input.playerIndex;

        //Set test colors of the tentacle
        leftTentacle.spriteShapeRenderer.color = testPlayerColors[playerIndex];
        rightTentacle.spriteShapeRenderer.color = testPlayerColors[playerIndex];

        //Move to the player position
        transform.position = Vector3.zero;

        //Move tentacle randomly on the player
        leftTentacle.tentaclePoint.transform.position = PlayerController.instance.transform.position + (Random.onUnitSphere * leftTentacle.minExpendSize);
        rightTentacle.tentaclePoint.transform.position = PlayerController.instance.transform.position + (Random.onUnitSphere * rightTentacle.minExpendSize);

        //Attach joints
        leftTentacle.tentacleJoint = PlayerController.instance.gameObject.AddComponent<RelativeJoint2D>();
        rightTentacle.tentacleJoint = PlayerController.instance.gameObject.AddComponent<RelativeJoint2D>();

        leftTentacle.tentacleJoint.connectedBody = leftTentacle.tentaclePoint;
        rightTentacle.tentacleJoint.connectedBody = rightTentacle.tentaclePoint;

        //Init both tentacles
        leftTentacle.Initialize(this);
        rightTentacle.Initialize(this);
    }

    private void Update()
    {
        if (!CanBeControlled && input.inputIsActive)
            input.DeactivateInput();
        else if (CanBeControlled && !input.inputIsActive)
            input.ActivateInput();
    }
}
