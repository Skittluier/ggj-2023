using TMPro;
using ForestFrenzy.Managers;
using UnityEngine;
using UnityEngine.U2D;
using UnityEngine.InputSystem;
using System.Collections;

public class PhysicsTentacle : MonoBehaviour
{
    [SerializeField, Tooltip("The moving tentacle tip of this Tentacle")]
    internal Rigidbody2D tentaclePoint;

    [SerializeField, Tooltip("The Physics joint in control of the tentacle")]
    internal RelativeJoint2D tentacleJoint;

    [SerializeField, Tooltip("The collider of this tentacle.")]
    private CircleCollider2D circleCollider;

    [SerializeField, Tooltip("Controller of the tentacle sprite shape")]
    internal SpriteShapeController spriteShapeController;

    [SerializeField, Tooltip("Renderer of the tentacle sprite shape")]
    internal SpriteShapeRenderer spriteShapeRenderer;

    [SerializeField, Tooltip("Control point renderer")]
    internal SpriteRenderer controlPointRenderer;

    [SerializeField, Tooltip("Minimum expend size")]
    internal float minExpendSize = 0.2f;

    [SerializeField, Tooltip("Maximum expansion size")]
    internal float maxExpansionSize = 1.5f;

    [SerializeField, Tooltip("Determines what joystick takes input")]
    private Joystick inputJoystick;

    [SerializeField, Tooltip("Min max random width scaling for arms")]
    private Vector2 MinMaxWidthScaling = new Vector2(0.4f, 0.5f);

    [Header("Grabbing")]

    [SerializeField, Tooltip("Size for collision checks during grabbing")]
    internal float grabSphereSize = 0.5f;

    [SerializeField, Tooltip("Layermask used for grabbing")]
    internal LayerMask grabLayerMask;

    [SerializeField, Tooltip("Audio effect played on grabbing")]
    internal AudioSource OnGrabAudio;

    [SerializeField, Tooltip("The different kind of grab effects")]
    internal AudioClip[] PossibleGrabAudioEffects;

    [SerializeField, Tooltip("Min-Max random audio pitch")]
    internal Vector2 MinMaxGrabAudioPitch = new Vector2(0.9f, 1.1f);

    [SerializeField, Tooltip("Min-Max random audio pitch")]
    internal Vector2 MinMaxGrabAudioVolume = new Vector2(0.9f, 1.1f);

    [Header("Connection joint settings")]

    [SerializeField, Tooltip("Determines the joint forces over distance")]
    internal AnimationCurve jointForceOverDistance;

    [SerializeField, Tooltip("Determines the joint torque over distance")]
    internal AnimationCurve jointTorqueOverDistance;

    [SerializeField, Tooltip("Determines the correction scale over distance")]
    internal AnimationCurve correctionScaleOverDistance;

    [SerializeField, Tooltip("Sprite shapes for open/closed hands")]
    internal SpriteShape handOpenShape, handClosedShape;

    /// <summary>
    /// The player that is the owner of this PhysicsTentacle
    /// </summary>
    internal PlayerInputPart ownerPlayer;

    /// <summary>
    /// Joint used for grabbing the world, can be null if not grabbing
    /// </summary>
    internal TargetJoint2D grabJoint;

    /// <summary>
    /// Latest input direction of the player normalized*
    /// </summary>
    private Vector2 latestInputDirection = new Vector2(1, 0);

    /// <summary>
    /// Initializes this tentacle for given player
    /// </summary>
    internal void Initialize(PlayerInputPart owner)
    {
        ownerPlayer = owner;

        float r = Random.Range(MinMaxWidthScaling.x, MinMaxWidthScaling.y);

        spriteShapeController.spline.SetHeight(0, r);
        spriteShapeController.spline.SetHeight(1, r);

        ProcessMovementInput(Vector2.zero);


    }

    /// <summary>
    /// Disables the tentacle and plays the KAPOT animation.
    /// </summary>
    internal void Die()
    {
        if (!enabled)
            return;

        enabled = false;

        circleCollider.isTrigger = true;
        Destroy(tentacleJoint);

        Game.Instance.RetractLife();
    }

    /// <summary>
    /// Called each frame
    /// </summary>
    private void Update()
    {
        if (tentacleJoint == null || this.ownerPlayer == null)
            return;

        //Movement input
        Vector2 leftStickInput = ownerPlayer.input.actions["LeftStick"].ReadValue<Vector2>();
        Vector2 rightStickInput = ownerPlayer.input.actions["RightStick"].ReadValue<Vector2>();
        Vector2 inputVector = inputJoystick == Joystick.left ? leftStickInput : rightStickInput;

        //Store latest input direction
        if (inputVector != Vector2.zero)
            latestInputDirection = inputVector.normalized;

        //Update joint settings
        float t = (PlayerController.instance.transform.position - transform.position).magnitude;

        //If we are grabbing reverse the forces
        if (grabJoint != null)
            t = -t;

        //Get forces over 
        float force = jointForceOverDistance.Evaluate(t);
        float torque = jointTorqueOverDistance.Evaluate(t);
        float correctionScale = correctionScaleOverDistance.Evaluate(t);

        //Update joint settings
        tentacleJoint.maxForce = force;
        tentacleJoint.maxTorque = torque;
        tentacleJoint.correctionScale = correctionScale;

        //Process tentacle movement input
        ProcessMovementInput(inputVector);
        ProcessGrabInput();
    }

    /// <summary>
    /// Moves the tentacles based on user input
    /// </summary>
    private void ProcessMovementInput(Vector2 inputVector)
    {
        //Set input vector if input is present
        if (inputVector != Vector2.zero)
        {
            //Set offset point for tentacle to go towards
            tentacleJoint.linearOffset = tentacleJoint.transform.worldToLocalMatrix.MultiplyVector(inputVector * maxExpansionSize);
        }
        else
        {
            //Set offset point for tentacle to go towards
            tentacleJoint.linearOffset = tentacleJoint.transform.worldToLocalMatrix.MultiplyVector(latestInputDirection * minExpendSize);
        }

        //Set origin position to the player
        spriteShapeController.spline.SetPosition(0, transform.position);
        spriteShapeController.spline.SetPosition(1, PlayerController.instance.transform.position);

        Vector2 tangent = Vector2.Lerp(-(transform.position - PlayerController.instance.transform.position), inputVector * 0.5f, 0.75f).normalized * 1f;

        spriteShapeController.spline.SetRightTangent(0, tangent);
        spriteShapeController.spline.SetLeftTangent(0, tangent);
    }

    /// <summary>
    /// Controls grab input, and grabs/releases based on user input
    /// </summary>
    private void ProcessGrabInput()
    {
        //Grab input
        bool triggerInputPressed = inputJoystick == Joystick.left ? ownerPlayer.input.actions["LeftTrigger"].IsPressed() : ownerPlayer.input.actions["RightTrigger"].IsPressed();
        bool triggerInputPressedThisFrame = inputJoystick == Joystick.left ? ownerPlayer.input.actions["LeftTrigger"].WasPressedThisFrame() : ownerPlayer.input.actions["RightTrigger"].WasPressedThisFrame();

        //Not yet grabbing
        if (grabJoint == null)
        {
            if (triggerInputPressedThisFrame)
            {
                //Only allowed to grab shit 
                if (Physics2D.OverlapCircleNonAlloc(transform.position, grabSphereSize, new Collider2D[1], grabLayerMask) > 0)
                {
                    grabJoint = gameObject.AddComponent<TargetJoint2D>();
                    grabJoint.target = transform.position;
                    grabJoint.maxForce = 10000f;
                    grabJoint.frequency = 1000;

                    //Play grab audio
                    OnGrabAudio.clip = PossibleGrabAudioEffects[Random.Range(0, PossibleGrabAudioEffects.Length)];
                    OnGrabAudio.volume = Random.Range(MinMaxGrabAudioVolume.x, MinMaxGrabAudioVolume.y);
                    OnGrabAudio.pitch = Random.Range(MinMaxGrabAudioPitch.x, MinMaxGrabAudioPitch.y);
                    OnGrabAudio.Play();

                    spriteShapeController.spriteShape = handClosedShape;

                    StartCoroutine(DoGrabHaptics());
                }
            }

        }
        //Already grabbing
        else
        {
            //Remove the grab joint if not pressing anymore
            if (!triggerInputPressed)
            {
                spriteShapeController.spriteShape = handOpenShape;
                Destroy(grabJoint);

            }
        }
    }

    private void OnDisable()
    {
        StopCoroutine("DoGrabHaptics");
        Gamepad pad = (Gamepad)ownerPlayer.input.devices[0];

        if (pad != null)
            pad?.PauseHaptics();
    }

    private IEnumerator DoGrabHaptics()
    {
        //Haptics
        Gamepad pad = (Gamepad)ownerPlayer.input.devices[0];

        if (pad != null)
        {
            pad.SetMotorSpeeds(0.2f, 1f);
            pad.ResumeHaptics();
        }

        yield return new WaitForSeconds(0.25f);

        pad.PauseHaptics();
    }

    private enum Joystick
    {
        left,
        right
    }
}
