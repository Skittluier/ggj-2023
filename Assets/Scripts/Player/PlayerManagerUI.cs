using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerManagerUI : MonoBehaviour
{
    [SerializeField]
    private PlayerInputManager inputManager;

    [SerializeField]
    private GameObject[] playerIndicators;

    private void Update()
    {
        for (int i = 0; i < playerIndicators.Length; i++)
            playerIndicators[i].gameObject.SetActive((i + 1) <= inputManager.playerCount);
    }
}
