using ForestFrenzy.Utilities;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    /// <summary>
    /// Instance of the current PlayerController
    /// </summary>
    internal static PlayerController instance;

    [SerializeField, Tooltip("The sprites of the eyes")]
    internal SpriteRenderer[] eyeSprites;

    [SerializeField, Tooltip("The main rigidBody of the player")]
    internal Rigidbody2D mainRigidbody;

    [SerializeField, Tooltip("Al the wow audioss")]
    internal AudioClip[] wowAudios;

    [SerializeField, Tooltip("The wow audio source")]
    internal AudioSource wowAudioSource;

    [SerializeField, Tooltip("Minimum nagnitude required for playing wow audio")]
    internal float minMagnitudeForWowAudio = 2f;

    [SerializeField, Tooltip("Delay for the wow audio")]
    internal float minDelayForWowAudio = 5f;

    [SerializeField, Tooltip("Renderer of the mouth")]
    internal SpriteRenderer mouthRenderer;

    [SerializeField, Tooltip("onzin")]
    internal Vector2 minMaxMouthLerpSize;

    [Header("Blink")]

    [SerializeField, Tooltip("Min and max blink time")]
    private Vector2 minMaxBlinkTime;

    [SerializeField, Tooltip("Amount of time for a full blink")]
    private float blinkTime;

    [SerializeField, Tooltip("Size of the eyes in idle form")]
    private float eyeStandardSize = 0.03395193f;

    [Header("Impact Audio")]

    [SerializeField, Tooltip("Filtering layermask for impact audio")]
    internal LayerMask impactAudioMask;

    [SerializeField, Tooltip("Possible impact audio effects")]
    internal AudioClip[] impactAudios;

    [SerializeField, Tooltip("The AudioSource used for playing impact sounds")]
    internal AudioSource impactAudioSource;

    [SerializeField, Tooltip("Impact magnitude sampled on this curve result in volume output")]
    internal AnimationCurve impactAudioVolumeCurve;

    /// <summary>
    /// Timestamp for playing the last wow SFX
    /// </summary>
    internal float latestWowAudio = 0f;

    /// <summary>
    /// Latest blank 
    /// </summary>
    internal float latestBlinkTime = 0f;

    /// <summary>
    /// Audio array to fill
    /// </summary>
    internal float[] audioSamples = new float[128];

    private bool enableRandomPlayerSFXs;

    [SerializeField, Tooltip("All the timeline related voice lines.")]
    private AudioClip[] timelineVoiceLines;
    private int currentVoiceLineNo = -1;


    /// <summary>
    /// Called on the first active frame
    /// </summary>
    private void Awake()
    {
        instance = this;

        PlayerInputPart.CanBeControlled = true;
    }

    public void EnableRandomPlayerSFXs()
    {
        enableRandomPlayerSFXs = true;
    }

    public void DisableRandomPlayerSFXs()
    {
        enableRandomPlayerSFXs = false;
    }

    /// <summary>
    /// Sets the next voice line.
    /// </summary>
    public void SetClipToNextVoiceline()
    {
        currentVoiceLineNo++;
        wowAudioSource.clip = timelineVoiceLines[currentVoiceLineNo];
        wowAudioSource.Play();
    }

    /// <summary>
    /// Called each frame
    /// </summary>
    private void Update()
    {
        if (enableRandomPlayerSFXs)
        {
            Vector2 v = mainRigidbody.velocity;

            if (v.magnitude >= minMagnitudeForWowAudio && Time.time >= latestWowAudio)
            {
                latestWowAudio = Time.time + minDelayForWowAudio;
                wowAudioSource.clip = wowAudios[Random.Range(0, wowAudios.Length - 1)];
                wowAudioSource.Play();
            }
        }


        if (wowAudioSource.isPlaying)
        {
            //Get output data from the playing audio source
            wowAudioSource.clip.GetData(audioSamples, wowAudioSource.timeSamples);

            float accumAvg = 0;

            for (int i = 0; i < audioSamples.Length; i++)
                accumAvg += Mathf.Abs(audioSamples[i]);

            //Final accum avg calculation
            accumAvg /= audioSamples.Length;

            float y = Mathf.Lerp(minMaxMouthLerpSize.x, minMaxMouthLerpSize.y, accumAvg);
            mouthRenderer.transform.localScale = new Vector3(mouthRenderer.transform.localScale.x, y, mouthRenderer.transform.localScale.z);
        }

        if (Time.time >= latestBlinkTime)
        {
            latestBlinkTime = Time.time + Random.Range(minMaxBlinkTime.x, minMaxBlinkTime.y);

            StartCoroutine(Blink());

            IEnumerator Blink()
            {
                float startTime = Time.time;
                while ((startTime + (blinkTime * 0.5f)) >= Time.time)
                {
                    float t = (Time.time - startTime) / (blinkTime * 0.5f);
                    float scale = Mathf.Lerp(eyeStandardSize, 0f, t);

                    eyeSprites[0].transform.localScale = new Vector3(eyeSprites[0].transform.localScale.x, scale, eyeSprites[0].transform.localScale.z);
                    eyeSprites[1].transform.localScale = new Vector3(eyeSprites[0].transform.localScale.x, scale, eyeSprites[0].transform.localScale.z);

                    //Wait for end frame
                    yield return null;
                }


                startTime = Time.time;
                while ((startTime + (blinkTime * 0.5f)) >= Time.time)
                {
                    float t = (Time.time - startTime) / (blinkTime * 0.5f);
                    float scale = Mathf.Lerp(0f, eyeStandardSize, t);

                    eyeSprites[0].transform.localScale = new Vector3(eyeSprites[0].transform.localScale.x, scale, eyeSprites[0].transform.localScale.z);
                    eyeSprites[1].transform.localScale = new Vector3(eyeSprites[1].transform.localScale.x, scale, eyeSprites[1].transform.localScale.z);

                    //Wait for end frame
                    yield return null;
                }
            }
        }
    }

    public void OnPlayerConnected(PlayerInput player)
    {
    }

    public void OnPlayerDisconnected(PlayerInput player)
    {

    }

    /// <summary>
    /// Called when this object collides
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.IsInLayerMask(impactAudioMask))
        {
            impactAudioSource.clip = impactAudios[Random.Range(0, impactAudios.Length)];
            impactAudioSource.volume = impactAudioVolumeCurve.Evaluate(collision.relativeVelocity.magnitude);
            impactAudioSource.Play();
        }
    }

    /// <summary>
    /// Toggles the player controls.
    /// </summary>
    /// <param name="toggle">Toggle on/off</param>
    public void ToggleControls(bool toggle)
    {
        PlayerInputPart.CanBeControlled = toggle;
    }
}
