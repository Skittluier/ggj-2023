namespace ForestFrenzy
{
    using ForestFrenzy.Managers;
    using UnityEngine;

    public class InvisibleWall : MonoBehaviour
    {
        [SerializeField]
        private float xOffset = 0f;


        private void Update()
        {
            Vector3 newPos = Game.Instance.CameraController.Camera.WorldToViewportPoint(transform.position);
            newPos.x = xOffset;

            transform.position = Game.Instance.CameraController.Camera.ViewportToWorldPoint(newPos);
        }
    }
}
