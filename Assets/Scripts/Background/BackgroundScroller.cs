using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{
    public float scrollRate = 1.0f;

    private SpriteRenderer spriteRenderer;
    private Material mat;

    private void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        //mat = new Material(spriteRenderer.material.shader);

        //spriteRenderer.material = mat;
    }

    void Update()
    {
        spriteRenderer.material.mainTextureOffset = new Vector2(Camera.main.transform.position.x * scrollRate, 0f);
    }
}
