using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundParent : MonoBehaviour
{
    public float zOffset = -1f;

    [SerializeField]
    private SpriteRenderer[] backgroundSpriteRenderers;

    [SerializeField]
    private Color[] panicColors;


    void Update()
    {
        transform.position = Camera.main.transform.position + new Vector3(0f, 0f, zOffset);
    }

    /// <summary>
    /// Changes the BG to the panic colours.
    /// </summary>
    public void ChangeToPanicColor()
    {
        for (int i = 0; i < backgroundSpriteRenderers.Length; i++)
            backgroundSpriteRenderers[i].color = panicColors[i];
    }
}
