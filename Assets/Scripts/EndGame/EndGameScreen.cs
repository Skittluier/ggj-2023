using ForestFrenzy.Managers;
using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class EndGameScreen : MonoBehaviour
{
    public float totalTime;
    public Image image;
    public CanvasGroup canvasGroup;
    public AudioSource audioSource;
    public AnimationCurve alphaCurve;
    public AnimationCurve scaleCurve;

    [SerializeField]
    private InputActionAsset inputActionAsset;


    public void OnEnable()
    {
        StartCoroutine(Play());
    }

    private void Update()
    {
        if (inputActionAsset.FindAction("Start").WasPressedThisFrame())
            Game.Instance.Restart();
    }

    private IEnumerator Play()
    {
        audioSource.Play();

        var startTime = Time.realtimeSinceStartup;

        while ((Time.realtimeSinceStartup - startTime) < totalTime)
        {
            var t = Time.realtimeSinceStartup - startTime;

            var scale = scaleCurve.Evaluate(t);
            var alpha = alphaCurve.Evaluate(t);

            canvasGroup.alpha = alpha;
            image.transform.localScale = Vector3.one * scale;

            yield return new WaitForEndOfFrame();
        }
    }
}
