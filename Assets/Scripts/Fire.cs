namespace ForestFrenzy
{
    using ForestFrenzy.Managers;
    using ForestFrenzy.Utilities;
    using System.Collections;
    using UnityEngine;

    public class Fire : MonoBehaviour
    {
        [SerializeField, Tooltip("An animation curve of the flame when it's entering into the camera viewport.")]
        private AnimationCurve flameEnterAnimationCurve;

        [SerializeField, Tooltip("Flame enter duration.")]
        private float flameEnterDuration = 1;

        private bool doneAnimating = false;

        [SerializeField, Tooltip("The X-Offset of the flames.")]
        private float xOffset;

        [SerializeField, Tooltip("The layer mask the fire listens to to kill some tentacles.")]
        private LayerMask hitLayerMask;

        internal delegate void OnFireVisibleMethod();
        internal static OnFireVisibleMethod OnFireVisible;


        /// <summary>
        /// Animate the flame into the camera.
        /// </summary>
        private IEnumerator Start()
        {
            float currVal = 0;
            float? fromX = null;

            while (currVal < 1)
            {
                currVal += Time.deltaTime / flameEnterDuration;

                Vector3 newPos = Game.Instance.CameraController.Camera.WorldToViewportPoint(transform.position);
                if (!fromX.HasValue)
                    fromX = newPos.x;

                newPos.x = Mathf.Lerp(fromX.Value, xOffset, flameEnterAnimationCurve.Evaluate(currVal));
                transform.position = Game.Instance.CameraController.Camera.ViewportToWorldPoint(newPos);

                yield return null;
            }

            doneAnimating = true;

            Game.Instance.CameraController.SetPlayerControlsCamera(false);
            OnFireVisible?.Invoke();
        }

        /// <summary>
        /// Let the flame follow the camera.
        /// </summary>
        private void Update()
        {
            if (doneAnimating)
            {
                Vector3 newPos = Game.Instance.CameraController.Camera.WorldToViewportPoint(transform.position);
                newPos.x = xOffset;

                transform.position = Game.Instance.CameraController.Camera.ViewportToWorldPoint(newPos);
            }
        }

        /// <summary>
        /// When the fire triggers something, it checks if it's the hit layer mask. If so, kill the tentacle.
        /// </summary>
        /// <param name="other">The object its colliding with.</param>
        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject.IsInLayerMask(hitLayerMask))
                if (other.gameObject.TryGetComponent(out PhysicsTentacle physicsTentacle))
                    physicsTentacle.Die();
        }
    }
}