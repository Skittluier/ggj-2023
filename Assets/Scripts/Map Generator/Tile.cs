using CombatTested.Pools;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class Tile : MonoBehaviour, IPoolable
{
    public int SubsequentlyIndex;
    public int Score;

    public int[] SubsequentlyTile;

    [SerializeField]
    private GameObject gameObject;
    public GameObject GameObject { get => gameObject; set => gameObject = value; }

    [SerializeField]
    private UnityEvent onFetchedFromPool;
    public UnityEvent OnFetchedFromPool { get => onFetchedFromPool; set => onFetchedFromPool = value; }

    [SerializeField]
    private bool disableOnReturnPool;
    public bool DisableOnReturnPool { get => disableOnReturnPool; set => disableOnReturnPool = value; }

    [field: SerializeField]
    public Transform StartPoint { get; private set; }

    [field: SerializeField]
    public Transform EndPoint { get; private set; }


    public void OnFetchFromPool()
    {
    }

    public void OnReturnedToPool()
    {
    }
}
