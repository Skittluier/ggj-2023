﻿using System.Collections.Generic;
using RUMBLE.Utilities;
using CombatTested.Pools;
using UnityEngine;
using ForestFrenzy.Managers;

public class TileManager : Singleton<TileManager>
{
    [SerializeField]
    private int activatedTileSize = 10;

    private Queue<Tile> Tiles = new Queue<Tile>();
    private Pool<Tile>[] tilePool;
    public Tile[] tile;
    private Tile lastTile;
    public List<Tile> activatedTileList = new List<Tile>();

    public SmoothFollowCamera cameraFollow;


    public void Start()
    {
        tilePool = new Pool<Tile>[tile.Length];

        for (int i = 0; i < tile.Length; i++)
            tilePool[i] = new Pool<Tile>(activatedTileSize * 2, activatedTileSize * 4, tile[i], PoolBehaviourType.ReturnFirst);

        for (int i = 0; i < activatedTileSize; i++)
            AddTileToQueue();
    }

    private void Update()
    {
        if (lastTile != null)
        {
            // Check if camera is at/under the maximum viewport space.
            // If so, spawn new tile.
            if (Game.instance.CameraController.Camera.WorldToViewportPoint(lastTile.transform.position).x <= 1)
                AddTileToQueue();
        }
    }

    public void AddTileToQueue()
    {
        Tile tile = null;

        if (lastTile == null)
        {
            tile = tilePool[0].FetchFromPool();

            tile.transform.position = transform.position - tile.StartPoint.localPosition;
            lastTile = tile;
        }
        else
        {
            int random = Random.Range(0, tilePool.Length);
            tile = tilePool[random].FetchFromPool();

            tile.transform.position = lastTile.EndPoint.position - tile.StartPoint.localPosition;

            lastTile = tile;
        }
        if (tile != null)
        {
            Tiles.Enqueue(tile);
        }
    }

    [ContextMenu("Delete Tile")]
    public void DeleteTileFromQueue()
    {
        if (Tiles.Count > 0)
        {
            Tile tile = Tiles.Dequeue();
            tilePool[tile.SubsequentlyIndex].MoveToPool(tile);
        }
    }

    public void TilesEnabled(Tile tiles)
    {
        activatedTileList.Add(tiles);
        UpdateCamera();
    }

    public void TilesDisable(Tile tile)
    {
        if (activatedTileList.Contains(tile))
        {
            activatedTileList.Remove(tile);
        }
    }

    public void UpdateCamera()
    {
        cameraFollow.UpdateXPosition(lastTile.transform.position.x);
    }

}
