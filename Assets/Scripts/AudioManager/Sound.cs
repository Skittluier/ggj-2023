using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Sound : MonoBehaviour
{
    public UnityEvent[] PlayEvent;

    [Button]
    public void PlayFirstEvent()
    {
        PlayAudio(0);
    }
    [Button]
    public void PlaySecondEvent()
    {
        PlayAudio(1);
    }



    public void PlayAudio(int i)
    {
        PlayEvent[i].Invoke();
    }
}
