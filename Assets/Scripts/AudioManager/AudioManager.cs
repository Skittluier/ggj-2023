using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public AudioSource audioSource;
    private float[] data = new float[64];
    public float amplitudeThreshold = 0.15f;
    public float amplitudeDecreaseRate = 0.5f;

    private SpriteRenderer render;
    public float amplitude;

    
    // Update is called once per frame
    void Update()
    {
        audioSource.clip.GetData(data, audioSource.timeSamples);

        float accumAvg = 0;

        for (int i = 0; i < data.Length; i++)
        {
            accumAvg += Mathf.Abs(data[i]);
        }


        accumAvg /= data.Length;

        if (accumAvg > amplitudeThreshold) 
            amplitude = Mathf.Max(amplitude, accumAvg);
        amplitude = Mathf.Max(0f, amplitude - Time.deltaTime * amplitudeDecreaseRate);
    }
}