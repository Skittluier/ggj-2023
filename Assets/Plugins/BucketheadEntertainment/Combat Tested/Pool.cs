﻿namespace CombatTested.Pools
{
    using UnityEngine;
    using UnityEngine.Events;
    using System.Collections.Generic;

    public class Pool<T> where T : IPoolable
    {
        //List containing all the items in the pool.
        public List<T> PooledObjects;
        //the type of item stored in the pool
        private T poolItem;
        //Max size of the pool
        public int MaxSize;
        //Empty gameobject to store the pool under.
        private Transform poolParent;
        //Index value indicating the next object to fetch from the pool.
        private int nextObjectToFetch = 0;
        //Enum that controls the behaviour of how the pool handles an index going out bounds
        public PoolBehaviourType BehaviourType = PoolBehaviourType.ExtendSize;

        /// <summary>
        /// Constructor.
        /// </summary>
        public Pool(int size, int maxSize, T poolObject, PoolBehaviourType type)
        {
            InstantiatePool(size, maxSize, poolObject);
            BehaviourType = type;
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public Pool(int size, int maxSize, T poolObject, Transform parent, PoolBehaviourType type)
        {
            poolParent = parent;
            InstantiatePool(size, maxSize, poolObject);
            BehaviourType = type;
        }

        /// <summary>
        /// Gets item returned on index.
        /// </summary>
        public T this[int index]
        {
            get
            {
                return PooledObjects[index];
            }
            set
            {
                PooledObjects[index] = value;
            }

        }

        /// <summary>
        /// Instaniates a pool of items.
        /// </summary>
        public void InstantiatePool(int size, int maxSize, T poolObject)
        {
            if (size < 0)
            {
                Debug.LogError("Size should be atleast 0!");
            }

            if (size > maxSize)
            {   Debug.LogError("The size should not be higher then the max size!");
            }

            poolItem = poolObject;
            PooledObjects = new List<T>(size);
            MaxSize = maxSize;

            AddItems(0, size);
        }

        /// <summary>
        /// Instaniates a pool of items that are children of the pool parent.
        /// </summary>
        public void InstantiatePool(int size, int maxSize, Transform poolParent)
        {
            if (size < 0)
            {
                Debug.LogError("Size should be atleast 0!");
            }

            if (size > maxSize)
            {
                Debug.LogError("The size should not be higher then the max size!");
            }

            if (poolParent.childCount <= 0)
            {
                Debug.LogError("The pool objects array can't be empty!");
            }

            int poolParentChildCount = poolParent.childCount;
            PooledObjects = new List<T>();

            for (int i = 0; i < poolParentChildCount; i++)
            {
                PooledObjects.Add(poolParent.GetChild(i).GetComponent<T>());
            }

            MaxSize = maxSize;
            this.poolParent = poolParent;
        }

        /// <summary>
        /// Adds new items to the pool at index: start and amount: amount
        /// </summary>
        public void AddItems(int start, int amount)
        {
            if ((start + amount) > MaxSize)
            {
                Debug.LogError("The preferred size is great then the max size!" + start + " " + amount + " " + MaxSize);
            }

            for (int i = 0; i < amount; i++)
                AddItem();
        }

        /// <summary>
        /// Adds an item to the pool.
        /// </summary>
        public void AddItem()
        {
            if (this.PooledObjects.Count == MaxSize && BehaviourType != PoolBehaviourType.ExtendSize)
            {
                Debug.LogError("The pool" + poolItem.ToString() + " is at maximum capacity.");
                return;
            }

            T poolObject = CreateNewPoolable();

            this.PooledObjects.Add(poolObject);

            poolObject.GameObject.SetActive(false);
            poolObject.GameObject.transform.parent = poolParent;
        }

        /// <summary>
        /// Fetches an item from the pool at given index, with desired rotation and position.
        /// </summary>
        public T FetchFromPool(int index, Vector3 position, Quaternion rotation)
        {
            T toReturn = FetchFromPool(index);

            toReturn.GameObject.transform.position = position;
            toReturn.GameObject.transform.rotation = rotation;
            toReturn.GameObject.SetActive(true);

            return toReturn;
        }

        /// <summary>
        /// Fetches an item from the pool at given index, with desired rotation and position.
        /// </summary>
        public T FetchFromPool(Vector3 position, Quaternion rotation)
        {
            int index = nextObjectToFetch;

            if (nextObjectToFetch >= PooledObjects.Count)
            {
                Debug.LogWarning("nextObjecToFetch exceeded pool count.");
                nextObjectToFetch = 0;
            }

            T toReturn = FetchFromPool(index);

            toReturn.GameObject.transform.position = position;
            toReturn.GameObject.transform.rotation = rotation;

            nextObjectToFetch++;

            return toReturn;
        }

        /// <summary>
        /// Fetches an item from the pool at given index.
        /// </summary>
        public T FetchFromPool(int index)
        {
            T toReturn;

            if (index < PooledObjects.Count)
                toReturn = PooledObjects[index];
            else
            {
                switch (BehaviourType)
                {
                    case PoolBehaviourType.ExtendSize:
                        AddItem();
                        toReturn = PooledObjects[index];
                        break;
                    default:
                    case PoolBehaviourType.ReturnFirst:
                        toReturn = PooledObjects[0];
                        break;

                }
            }

            if (toReturn.OnFetchedFromPool != null)
                toReturn.OnFetchedFromPool.Invoke();

            toReturn.OnFetchFromPool();

            return toReturn;
        }

        /// <summary>
        /// Fetches an item from the pool 
        /// </summary>
        public T FetchFromPool()
        {
            int index = nextObjectToFetch;

            if (nextObjectToFetch >= PooledObjects.Count)
            {
                Debug.LogWarning("nextObjecToFetch exceeded pool count.");
                nextObjectToFetch = 0;
            }

            T toReturn = FetchFromPool(index);
            nextObjectToFetch++;

            toReturn.OnFetchFromPool();

            return toReturn;
        }

        /// <summary>
        /// Creates a new object based in poolItem
        /// </summary>
        private T CreateNewPoolable()
        {
            return Object.Instantiate(this.poolItem.GameObject, new Vector3(10, -10, PooledObjects.Count), Quaternion.identity).GetComponent<T>();
        }

        /// <summary>
        /// Sets an item back to the pool.
        /// </summary>
        public void MoveToPool(T item)
        {
            if (item.DisableOnReturnPool)
                item.GameObject.SetActive(false);

            item.GameObject.transform.position = Vector3.zero;
            item.GameObject.transform.rotation = Quaternion.identity;

            item.OnReturnedToPool();
        }

        /// <summary>
        /// Sets an item with a specific index to the pool.
        /// </summary>
        /// <param name="itemNo">Item number</param>
        public void MoveToPool(int itemNo)
        {
            if (itemNo < 0 || itemNo >= PooledObjects.Count)
            {
                Debug.LogError("This item is not in the pool!");
                return;
            }

            if (PooledObjects[itemNo].DisableOnReturnPool)
                PooledObjects[itemNo].GameObject.SetActive(false);
            //else
            //    PooledObjects[itemNo].GameObject.transform.position = new Vector3(10, -10, 10);

            PooledObjects[itemNo].OnReturnedToPool();
        }
    }
    public enum PoolBehaviourType
    {
        ExtendSize,
        ReturnFirst,
    }

    public interface IPoolable
    {
        GameObject GameObject
        {
            get;
        }
        UnityEvent OnFetchedFromPool
        {
            get;
        }
        bool DisableOnReturnPool
        {
            get;
            set;
        }

        void OnFetchFromPool();
        void OnReturnedToPool();
    }
}