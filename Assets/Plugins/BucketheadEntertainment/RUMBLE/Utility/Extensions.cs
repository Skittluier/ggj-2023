﻿using UnityEngine.Events;

public static class Extensions
{
    /// <summary>
    /// Adds a listener to a Unity event which can't be duplicated.
    /// Everytime you're calling this method, the UnityEvent listener removes the UnityAction and adds it.
    /// </summary>
    /// <param name="unityEvent">The Unity event itself</param>
    /// <param name="unityAction">The Unity Action you want to subscribe</param>
    public static void AddNonDuplicatedListener(this UnityEvent unityEvent, UnityAction unityAction)
    {
        unityEvent.RemoveListener(unityAction);
        unityEvent.AddListener(unityAction);
    }

    /// <summary>
    /// Adds a listener to a Unity event which can't be duplicated.
    /// Everytime you're calling this method, the UnityEvent listener removes the UnityAction and adds it.
    /// </summary>
    /// <param name="unityEvent">The Unity event itself</param>
    /// <param name="unityAction">The Unity Action you want to subscribe</param>
    public static void AddNonDuplicatedListener<T>(this UnityEvent<T> unityEvent, UnityAction<T> unityAction)
    {
        unityEvent.RemoveListener(unityAction);
        unityEvent.AddListener(unityAction);
    }
}
