﻿public enum HeadsetFamily
{
    Oculus,
    Vive,
    WMR,
    Unknown
}

public enum ControllerType
{
    Touch,
    WMR,
    ViveWand,
    Index,
    Unknown
}