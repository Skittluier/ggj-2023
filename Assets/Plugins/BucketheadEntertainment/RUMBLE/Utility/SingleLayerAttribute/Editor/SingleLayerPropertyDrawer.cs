﻿using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

[CustomPropertyDrawer(typeof(SingleLayerAttribute))]
public class SingleLayerPropertyDrawer : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        EditorGUILayout.BeginHorizontal();

        EditorGUILayout.LabelField(label);
        property.intValue = EditorGUILayout.Popup(property.intValue, GetPhsyicsLayers());
        EditorGUILayout.EndHorizontal();
        EditorGUI.EndProperty();
    }


    private string[] GetPhsyicsLayers()
    {
        List<string> names = new List<string>();

        for (int i = 0; i < 32; i++)
        {
            string name = InternalEditorUtility.GetLayerName(i);

            if ((!string.IsNullOrEmpty(name) && i > 7) || i <= 7)
                names.Add(name);
        }

        return names.ToArray();
    }
}
