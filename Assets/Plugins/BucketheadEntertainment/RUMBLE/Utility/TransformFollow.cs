﻿using RUMBLE.Utilities;
using UnityEngine;

public class TransformFollow : MonoBehaviour
{
    [SerializeField,Tooltip("Should the position of transformToFollow be followed ?")]
    private bool followPosition = true;
    public bool FollowPosition
    {
        get
        {
            return followPosition;
        }
        set
        {
            followPosition = value;
        }
    }

    [SerializeField, Tooltip("Should the rotation of transformToFollow be followed ?")]
    private bool followRotation = true;
    public bool FollowRotation
    {
        get
        {
            return followRotation;
        }
        set
        {
            followRotation = value;
        }
    }

    [SerializeField,Tooltip("The transform to copy position/rotation from")]
    private Transform transformToFollow;
    public Transform TransformToFollow
    {
        get
        {
            return transformToFollow;
        }
        set
        {
            transformToFollow = value;
        }
    }

    [SerializeField,Tooltip("Determines when the syncing of position/rotation is called")]
    private UpdateMode updateMode = UpdateMode.Update;
    public UpdateMode UpdateMode
    {
        get
        {
            return updateMode;
        }
        set
        {
            updateMode = value;
        }
    }

    /// <summary>
    /// Called each frame
    /// </summary>
    private void Update()
    {
        if (UpdateMode == UpdateMode.Update)
            FollowTransform();
    }

    /// <summary>
    /// Called at each fixed timestep (Physics timestep)
    /// </summary>
    private void FixedUpdate()
    {
        if (UpdateMode == UpdateMode.FixedUpdate)
            FollowTransform();
    }

    /// <summary>
    /// Called after each frame
    /// </summary>
    private void LateUpdate()
    {
        if (UpdateMode == UpdateMode.LateUpdate)
            FollowTransform();
    }

    /// <summary>
    /// Called before each render ca;;
    /// </summary>
    private void OnPreRender()
    {
        if (UpdateMode == UpdateMode.OnPreRender)
            FollowTransform();
    }

    /// <summary>
    /// Sync the marked properties of given transform over to this transform
    /// </summary>
    public void FollowTransform()
    {
        if (transformToFollow)
        {
            if (followPosition)
                transform.position = transformToFollow.transform.position;

            if (followRotation)
                transform.rotation = transformToFollow.transform.rotation;
        }
    }
}

public enum UpdateMode
{
    Update,
    LateUpdate,
    FixedUpdate,
    OnPreRender,
    Manual
}

