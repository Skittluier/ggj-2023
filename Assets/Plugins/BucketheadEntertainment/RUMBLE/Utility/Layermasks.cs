﻿using UnityEngine;

namespace RUMBLE.Utilities
{
    public class Layermasks : Singleton<Layermasks>
    {
        [SerializeField,Tooltip("This mask is used to determine all InteractionBase objects")]
        private LayerMask interactionBase = ~0;
        public LayerMask InteractionBase => interactionBase;

        [SerializeField,Tooltip("This mask is used to determine the body of the player")]
        private LayerMask playerBodyMask = ~0;
        public LayerMask PlayerBodyMask => playerBodyMask;

        [SerializeField,Tooltip("This mask is used to determine environment objects")]
        private LayerMask environmentMask = ~0;
        public LayerMask EnvironmentMask => environmentMask;

        [SerializeField, Tooltip("TEMP")]
        private LayerMask playerLayermask = ~0;
        public LayerMask PlayerLayermask => playerLayermask;

        [SerializeField, Tooltip("This mask is used to determine all Move layers")]
        private LayerMask moveLayermask = ~0;
        public LayerMask MoveLayermask => moveLayermask;

        [SerializeField, Tooltip("This mask is used to determine all layers which are allowed to be used to spawn moves")]
        private LayerMask moveSpawnLayermask = ~0;
        public LayerMask MoveSpawnLayermask => moveSpawnLayermask;

        [SerializeField, Tooltip("This mask is used to determine all layers which are part of the ground")]
        private LayerMask groundLayerMask = ~0;
        public LayerMask GroundLayerMask => groundLayerMask;
    }
}