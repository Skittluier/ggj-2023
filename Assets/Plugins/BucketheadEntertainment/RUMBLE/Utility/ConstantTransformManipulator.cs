using UnityEngine;

namespace RUMBLE.Utilities
{
    public class ConstantTransformManipulator : MonoBehaviour
    {
        public enum UpdateType
        {
            Update, LateUpdate
        }

        [SerializeField, Tooltip("The transform being manipulated."), Header("References")]
        private Transform manipulatedTransform;

        [SerializeField, Tooltip("Should the transform rotate constantly?"), Header("Rotation Settings")]
        private bool useRotator;

        [SerializeField, Tooltip("What's the Vector it should rotate in?")]
        private Vector3 rotationVector;

        [SerializeField, Tooltip("The constant rotation angle.")]
        private float rotationAngle;

        [SerializeField, Tooltip("What's the Update loop this should happen in?")]
        private UpdateType rotateUpdateType;

        [SerializeField, Tooltip("Should it rotate in its local space?")]
        private Space rotationSpace;

        [SerializeField, Tooltip("Should the transform move constantly?"), Header("Position Settings")]
        private bool useMover;

        [SerializeField, Tooltip("What's the Vector it should move to?")]
        private Vector3 movementVector;

        [SerializeField, Tooltip("The constant movement speed.")]
        private float movementMagnitude;

        [SerializeField, Tooltip("What's the Update loop this should happen in?")]
        private UpdateType movementUpdateType;

        [SerializeField, Tooltip("Should it position in its local space?")]
        private Space movementSpace;


        private void Awake()
        {
            if (!manipulatedTransform)
                manipulatedTransform = transform;
        }

        private void Update()
        {
            if (useRotator && rotateUpdateType == UpdateType.Update)
                transform.Rotate(rotationVector, rotationAngle * Time.deltaTime, rotationSpace);

            if (useMover && movementUpdateType == UpdateType.Update)
                transform.Translate(movementVector * movementMagnitude, movementSpace);
        }

        private void LateUpdate()
        {
            if (useRotator && rotateUpdateType == UpdateType.LateUpdate)
                transform.Rotate(rotationVector, rotationAngle * Time.deltaTime, rotationSpace);

            if (useMover && movementUpdateType == UpdateType.LateUpdate)
                transform.Translate(movementVector * movementMagnitude, movementSpace);
        }
    }
}
