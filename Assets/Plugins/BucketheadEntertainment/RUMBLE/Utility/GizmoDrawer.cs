﻿using UnityEngine;
using System.Collections;
using RUMBLE.Utilities;
using System.Collections.Generic;
using RUMBLE.Utilities.Timers;

/// <summary>
/// Helper class to drawm gizmos from outside of a MonoBehaviour object
/// </summary>
public class GizmoDrawer : SelfSpawningSingleton<GizmoDrawer>
{
    private List<Gizmo> gizmosToDraw = new List<Gizmo>();

    public void AddSphereGizmo(Vector3 position, Quaternion rot, float radius, Color color, DrawType drawType = DrawType.Solid, float aliveTime = 0)
    {
        SphereGizmo sg = new SphereGizmo(position, rot, color, radius, drawType);

        if (aliveTime != 0)
        {
            TimerManager.AutoDestructTimer(aliveTime, () =>
            {
                if (sg != null && gizmosToDraw != null)
                    gizmosToDraw.Remove(sg);

            });
        }

        gizmosToDraw.Add(sg);
    }

    public void AddBoxGizmo(Vector3 position, Quaternion rot, Vector3 size, Color color, DrawType drawType = DrawType.Solid, float aliveTime = 0)
    {
        BoxGizmo bg = new BoxGizmo(position, rot, size, color, drawType);

        if (aliveTime != 0)
        {
            TimerManager.AutoDestructTimer(aliveTime, () =>
            {
                if (bg != null && gizmosToDraw != null)
                    gizmosToDraw.Remove(bg);

            });
        }

        gizmosToDraw.Add(bg);
    }

    public void AddCapsuleGizmo(Vector3 fromPoint, Vector3 toPoint, Quaternion rot, float radius, Color color, DrawType drawType = DrawType.Solid, float aliveTime = 0)
    {
        CapsuleGizmo cg = new CapsuleGizmo(fromPoint, toPoint, rot, color, radius, drawType);

        if (aliveTime != 0)
        {
            TimerManager.AutoDestructTimer(aliveTime, () =>
            {
                if (cg != null && gizmosToDraw != null)
                    gizmosToDraw.Remove(cg);

            });
        }

        gizmosToDraw.Add(cg);
    }

    public void OnDrawGizmos()
    {
        for (int i = 0; i < gizmosToDraw.Count; i++)
            gizmosToDraw[i].Draw();
    }


    public abstract class Gizmo
    {
        public Gizmo(Vector3 position, Color color, Quaternion rot, DrawType drawtype = DrawType.Solid)
        {
            Color = color;
            Position = position;
            DrawType = drawtype;
            Rotation = rot;
        }

        public Quaternion Rotation;
        public DrawType DrawType;
        public Color Color;
        public Vector3 Position;
        public abstract void Draw();
    }

    public class SphereGizmo : Gizmo
    {
        public SphereGizmo(Vector3 position, Quaternion rotation, Color color, float radius, DrawType drawtype = DrawType.Solid) : base(position, color, rotation, drawtype)
        {
            Radius = radius;
        }

        public float Radius;

        public override void Draw()
        {
            Gizmos.color = Color;
            Gizmos.matrix = Matrix4x4.TRS(Position, Rotation, Vector3.one);


            if (DrawType == DrawType.Solid)
                Gizmos.DrawSphere(Vector3.zero, Radius);
            else if (DrawType == DrawType.Wireframe)
                Gizmos.DrawWireSphere(Vector3.zero, Radius);

            Gizmos.matrix = Matrix4x4.identity;
        }
    }

    public class BoxGizmo : Gizmo
    {
        public BoxGizmo(Vector3 position, Quaternion rotation, Vector3 size, Color color, DrawType drawtype = DrawType.Solid) : base(position, color, rotation, drawtype)
        {
            Size = size;
        }

        public Vector3 Size;

        public override void Draw()
        {
            Gizmos.color = Color;

            Gizmos.matrix = Matrix4x4.TRS(Position, Rotation, Vector3.one);

            if (DrawType == DrawType.Solid)
                Gizmos.DrawCube(Vector3.zero, Size);
            else if (DrawType == DrawType.Wireframe)
                Gizmos.DrawWireCube(Vector3.zero, Size);

            Gizmos.matrix = Matrix4x4.identity;
        }
    }

    public class CapsuleGizmo : Gizmo
    {
        public CapsuleGizmo(Vector3 position, Vector3 toPoint, Quaternion rotation, Color color, float radius, DrawType drawType = DrawType.Solid) : base(position, color, rotation, drawType)
        {
            ToPoint = toPoint;
            Radius = radius;
        }

        public Vector3 ToPoint;
        public float Radius;

        public override void Draw()
        {
            Gizmos.color = Color;
            Gizmos.matrix = Matrix4x4.TRS(Position, Rotation, Vector3.one);

            if (DrawType == DrawType.Solid)
                Gizmos.DrawSphere(Vector3.zero, Radius);
            else if (DrawType == DrawType.Wireframe)
                Gizmos.DrawWireSphere(Vector3.zero, Radius);

            Vector3 toPoint = ToPoint - Position;

            Gizmos.DrawLine(Vector3.left * Radius, toPoint + Vector3.left * Radius);
            Gizmos.DrawLine(Vector3.right * Radius, toPoint + Vector3.right * Radius);
            Gizmos.DrawLine(Vector3.forward * Radius, toPoint + Vector3.forward * Radius);
            Gizmos.DrawLine(Vector3.back * Radius, toPoint + Vector3.back * Radius);

            if (DrawType == DrawType.Solid)
                Gizmos.DrawSphere(toPoint, Radius);
            else if (DrawType == DrawType.Wireframe)
                Gizmos.DrawWireSphere(toPoint, Radius);

            Gizmos.matrix = Matrix4x4.identity;
        }
    }

    public enum DrawType
    {
        Solid,
        Wireframe
    }
}

