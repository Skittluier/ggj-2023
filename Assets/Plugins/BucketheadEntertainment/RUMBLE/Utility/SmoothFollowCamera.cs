﻿using System;
using UnityEngine;

public class SmoothFollowCamera : MonoBehaviour
{
    [SerializeField]
    private Camera followingCamera;

    [SerializeField]
    private GameObject cameraToFollow;

    private Vector3 velocity;

    [SerializeField]
    private float smoothTime;

    [SerializeField]
    private float rotationSpeed;

    private void Start()
    {
        cameraToFollow.transform.position = followingCamera.transform.position;
    }

    private void Update()
    {
        followingCamera.transform.position = Vector3.SmoothDamp(followingCamera.transform.position, cameraToFollow.transform.position, ref velocity, smoothTime);
        followingCamera.transform.rotation = Quaternion.Slerp(followingCamera.transform.rotation, cameraToFollow.transform.rotation, rotationSpeed * Time.deltaTime);
    }

    public void UpdateXPosition(float xPos)
    {
        float currentXpos = followingCamera.transform.position.x;
        cameraToFollow.transform.position = new Vector3(xPos, followingCamera.transform.position.y,followingCamera.transform.position.z);
    }
}
