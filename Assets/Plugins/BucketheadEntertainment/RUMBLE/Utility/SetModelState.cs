﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;

namespace RUMBLE.Utilities
{
    /// <summary>
    /// Little helper script to force 3D models into poses
    /// </summary>
    public class SetModelState : MonoBehaviour
    {
        [SerializeField]
        public Transform rootTransform;

        [SerializeField, HideInInspector]
        private List<BoneState> boneStates;

        [ContextMenu("Set reference state")]
        public void SetReferenceModel()
        {
            if (!rootTransform)
            {
                Debug.LogError("No RootTransform assigned!");
                return;
            }
            boneStates = new List<BoneState>();

            AddPoseRecursevily(rootTransform, boneStates);
        }

        private void AddPoseRecursevily(Transform tr, List<BoneState> poses)
        {
            poses.Add(new BoneState()
            {
                Transform = tr,
                LocalPosition = tr.localPosition,
                LocalRotation = tr.localRotation
            });

            int childCount = tr.childCount;

            for (int i = 0; i < childCount; i++)
                AddPoseRecursevily(tr.GetChild(i), poses);
        }

        [ContextMenu("Set model to reference state")]
        public void SetModelToReferenceState()
        {
            if (boneStates == null)
            {
                Debug.LogError("No BoneStates, please set a reference state first!");
                return;
            }

            for (int i = 0; i < boneStates.Count; i++)
            {
                boneStates[i].Transform.localPosition = boneStates[i].LocalPosition;
                boneStates[i].Transform.localRotation = boneStates[i].LocalRotation;
            }
        }
    }

    public struct BoneState
    {
        public Transform Transform;
        public Vector3 LocalPosition;
        public Quaternion LocalRotation;
    }
}
#endif