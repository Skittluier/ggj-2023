﻿using UnityEngine;

namespace RUMBLE.Utilities
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected bool ShouldBeDestroyed { get; private set; }

        protected static T instance;
        public static T Instance
        {
            get
            {
                return instance;
            }
            protected set
            {
                instance = value;
            }
        }

        [SerializeField]
        protected bool dontDestroyOnLoad, ignoreErrorWhenDuplicatedInstance;


        /// <summary>
        /// Defines the instance of the singleton.
        /// </summary>
        protected virtual void Awake()
        {
            T thisInstance = this as T;

            if (instance && instance != thisInstance)
            {
                ShouldBeDestroyed = true;

                Destroy(thisInstance.gameObject);

                if (!ignoreErrorWhenDuplicatedInstance)
                    Debug.LogErrorFormat("Duplicate Singleton instance found of type: {0} destroying instance.", null, thisInstance.ToString());
                return;
            }

            instance = thisInstance;

            if (dontDestroyOnLoad)
            {
                transform.parent = null;
                DontDestroyOnLoad(gameObject);
            }
        }

        /// <summary>
        /// Forgets the instance and makes the instance null.
        /// </summary>
        public static void ForgetInstance()
        {
            instance = null;
        }

        /// <summary>
        /// This looks for the selected singleton and assigns the singleton manually.
        /// </summary>
        public static void FindInstance()
        {
            Singleton<T> instance = FindObjectOfType<Singleton<T>>();

            if (instance)
                instance.Awake();
        }
    }
}