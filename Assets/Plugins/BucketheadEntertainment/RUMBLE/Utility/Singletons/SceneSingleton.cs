﻿namespace RUMBLE.Utilities
{
    using UnityEngine;

    /// <summary>
    /// Singleton implementation for singletons that are present for the duration of a scene load (aka not persisent)
    /// </summary>
    public class SceneSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance;
        /// <summary>
        /// Singleton instance
        /// </summary>
        public static T Instance
        {
            get
            {
                return instance;
            }
        }

        /// <summary>
        /// Called at the first active frame, used for setting singleton 
        /// </summary>
        protected virtual void Awake()
        {
            //Just set the instance
            instance = this as T;
        }

    }
}
