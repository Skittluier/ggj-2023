﻿using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace RUMBLE.Utilities
{
    [System.Serializable]
    public class ScriptableObjectSingleton<T> : ScriptableObject where T : ScriptableObject
    {
        //Singleton instance
        private static T instance;
        public static T Instance
        {
            get
            {
#if UNITY_EDITOR
                //If there is no instance present in Editor, we fetch is automatically
                if (instance == null)
                    FetchEditorInstance();
#endif
                if (instance == null)
                    Debug.LogWarning("No instance for ScriptableObjectSingleton found!");

                return instance;
            }
            set
            {
                instance = value;
            }
        }

#if UNITY_EDITOR
        /// <summary>
        /// Fetches the instance of this ScriptableObject in Editor, this is different because Awake is only called once in Editor, not every game start
        /// </summary>
        private static void FetchEditorInstance()
        {
            string typeName = typeof(T).Name;
            string[] guids = AssetDatabase.FindAssets("t:" + typeName);

            //No instances found in Assets
            if (guids.Length == 0)
            {
                Debug.LogErrorFormat("[ScriptableObjectSingleton] Failed to retrieve Asset of type: {0}, please make sure there is 1 in the project", null, typeName);
                return;
            }
            else if (guids.Length > 1)
            {
                Debug.LogFormat("[ScriptableObjectSingleton] Multiple Assets of type: {0} loading first available, please make sure there is only 1 in the project", null, typeName);
                return;
            }

            //Set the instance as the first available
            instance = AssetDatabase.LoadAssetAtPath<T>(AssetDatabase.GUIDToAssetPath(guids[0]));
        }
#else
        /// <summary>
        /// This gets called when this object is loaded, this is used for setting the instance in build
        /// </summary>
        private void Awake()
        {
            instance = this as T;
        }
#endif
        /// <summary>
        /// Called when this object is disabled
        /// </summary>
        public void OnDisable()
        {
            instance = null;
        }
    }
}