﻿using UnityEngine;

namespace RUMBLE.Utilities
{
    /// <summary>
    /// Singleton class that spawns itself when it's not already present in the scene (Don't use this class in production)
    /// </summary>
    public class SelfSpawningSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        //Singleton instance
        protected static T instance;
        public static T Instance
        {
            get
            {
                if(instance == null)
                    instance = FindObjectOfType<T>();

                if(instance == null)
                {
                    GameObject go = new GameObject("SelfSpawningSingleton<" + typeof(T).ToString() + ">");
                    instance = go.AddComponent<T>();
                }

                return instance;
            }
            set
            {
                instance = value;
            }
        }

        [SerializeField,Tooltip("Determines if this object is persistent between scenes.")]
        protected bool dontDestroyOnLoad = false;
        
        /// <summary>
        /// Called once on the first active frame
        /// </summary>
        protected virtual void Awake()
        {
            if (dontDestroyOnLoad)
                DontDestroyOnLoad(this);
        }
    }
}