﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace RUMBLE.Utilities.Timers
{
    public class TimerManager : ScriptableObject
    {
        //Singleton instance.
        private static TimerManager _instance;
        public static TimerManager Instance
        {
            get
            {
                if (_instance == null)
                    _instance = FindObjectOfType<TimerManager>();

                if (_instance == null)
                    _instance = CreateInstance<TimerManager>();

                return _instance;
            }
            set
            {
                if (_instance != null)
                {
                    Debug.LogError("TimerManager is already defined.", null);
                    return;
                }

                Debug.Log("New TimerManager has been created.", null);

                _instance = value;

            }
        }

        //List containing all timers.
        private List<Timer> allTimers = new List<Timer>();
        public List<Timer> AllTimers => allTimers;

        //Timers to remove from the list.
        private List<Timer> timersToRemove = new List<Timer>();

        #region Timer Create functions.

        /// <summary>
        /// Creates a self destructing timer, only use this for calling a function after a duration
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="onTimerEnd">Function to call when the timer has ended.</param>
        public static void AutoDestructTimer(float duration, Timer.OnTimerEnd onTimerEnd, bool isPersistent = false)
        {
            AutoDestructTimer(duration, "New Autodestruct Timer", onTimerEnd, isPersistent);
        }

        /// <summary>
        /// Creates a self destructing timer, only use this for calling a function after a duration
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="onTimerEnd">Function to call when the timer has ended.</param>
        public static void AutoDestructTimer(float duration, string Name, Timer.OnTimerEnd onTimerEnd, bool isPersistent = false)
        {
            Timer timer = new Timer(duration);
            timer.OnTimerEnded += onTimerEnd;
            timer.Name = Name;
            timer.OnTimerEnded += () => { timer.MarkForCleanUp = true; };
            timer.IsPersistent = isPersistent;
            Instance.AllTimers.Add(timer);
        }

        /// <summary>
        /// Creates a self destructing timer, only use this for calling a function after a duration
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="onTimerEnd">Function to call when the timer has ended.</param>
        public static void AutoDestructTimer(float duration, string Name, Timer.OnTimerStart onTimerStart, Timer.OnTimerTick onTimerTick, Timer.OnTimerEnd onTimerEnd, bool isPersistent = false)
        {
            Timer timer = new Timer(duration);
            timer.OnTimerEnded += onTimerEnd;
            timer.Name = Name;


            timer.OnTimerStarted += onTimerStart;
            timer.OnTimerTicked += onTimerTick;
            timer.OnTimerEnded += () => { timer.MarkForCleanUp = true; };

            timer.IsPersistent = isPersistent;

            Instance.AllTimers.Add(timer);

        }
        /// <summary>
        /// Creates and returns a timer.
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <returns>Created timer.</returns>
        public static Timer CreateTimer(float duration)
        {
            return CreateTimer(duration, string.Empty, null, null, null);
        }

        /// <summary>
        /// Creates and returns a timer.
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <returns>Created timer.</returns>
        public static Timer CreateTimer(float duration, string name)
        {
            return CreateTimer(duration, name, null, null, null);
        }

        /// <summary>
        /// Creates and returns a timer.
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="onTimerEnd">Function to invoke when timer is completed.</param>
        /// <returns>Created timer.</returns>
        public static Timer CreateTimer(float duration, Timer.OnTimerEnd onTimerEnd)
        {
            return CreateTimer(duration, string.Empty, null, null, onTimerEnd);
        }

        /// <summary>
        /// Creates and returns a timer.
        /// </summary>
        /// <param name="duration">Duration of the timer.</param>
        /// <param name="onTimerStart">Function to invoke when timer has started.</param>
        /// <param name="onTimerTick">Function to invoke when timer has ticked.</param>
        /// <param name="onTimerEnd">Function to invoke when timer is completed.</param>
        /// <returns></returns>
        public static Timer CreateTimer(float duration = 0f,
            string name = "",
            Timer.OnTimerStart onTimerStart = null,
            Timer.OnTimerTick onTimerTick = null,
            Timer.OnTimerEnd onTimerEnd = null)
        {
            Timer timer = new Timer(duration);

            if (onTimerStart != null)
                timer.OnTimerStarted += onTimerStart;

            if (onTimerTick != null)
                timer.OnTimerTicked += onTimerTick;

            if (onTimerEnd != null)
                timer.OnTimerEnded += onTimerEnd;

            if (!string.IsNullOrEmpty(name))
                timer.Name = name;

            Instance.allTimers.Add(timer);

            return timer;
        }
        #endregion

        /// <summary>
        /// Called every frame.
        /// </summary>
        public void Update()
        {
            if (Time.timeScale == 0)
                return;

            TickAllTimers();
        }

        /// <summary>
        /// Returns the amount of running timers.
        /// </summary>
        /// <returns></returns>
        public int RunnningTimers()
        {
            int amount = 0;

            for (int i = 0; i < Instance.AllTimers.Count; i++)
                if (!Instance.AllTimers[i].Done && !Instance.AllTimers[i].Paused)
                    amount++;

            return amount;
        }

        /// <summary>
        /// Returns the amount of paused timers.
        /// </summary>
        /// <returns></returns>
        public int PausedTimers()
        {
            int amount = 0;

            for (int i = 0; i < Instance.AllTimers.Count; i++)
                if (!Instance.AllTimers[i].Done && Instance.AllTimers[i].Paused)
                    amount++;

            return amount;
        }

        /// <summary>
        /// Returns the amount of stopped timers.
        /// </summary>
        public int StoppedTimers()
        {
            int amount = 0;

            for (int i = 0; i < Instance.AllTimers.Count; i++)
                if (Instance.AllTimers[i].Done)
                    amount++;

            return amount;
        }

        /// <summary>
        /// Clears all Timers that are not marked as persistent
        /// </summary>
        private static void ClearUnPersistentTimers()
        {
            for (int i = 0; i < Instance.allTimers.Count; i++)
            {
                if (!Instance.allTimers[i].IsPersistent)
                {
                    Instance.allTimers.RemoveAt(i);
                    i = 0;
                }
            }
        }

        /// <summary>
        /// Ticks all the 'not-ignored by profiler' timers if necessary
        /// </summary>
        private void TickAllTimers()
        {
            int currentIndex = -1;

            try
            {
                for (int i = 0; i < allTimers.Count; i++)
                {
                    currentIndex = i;

                    if (!allTimers[i].Paused)
                    {
                        bool status = allTimers[i].Tick(Time.deltaTime);

                        if (i >= allTimers.Count || allTimers[i] == null)
                            continue;

                        if (status)
                        {
                            if (i >= allTimers.Count)
                                break;

                            if (allTimers[i] != null && (allTimers[i].MarkForCleanUp || !allTimers[i].Recursive))
                                timersToRemove.Add(allTimers[i]);

                            if (allTimers[i].OnTimerEnded != null)
                                allTimers[i].OnTimerEnded.Invoke();

                            if (i >= allTimers.Count || allTimers[i] == null)
                                continue;

                            if (i < allTimers.Count && allTimers[i].Recursive)
                                allTimers[i].Reset();
                        }
                    }
                    else
                    {
                        //Check if this timer should be removed, even though it is pauzed
                        if (allTimers[i] != null && (allTimers[i].MarkForCleanUp || !allTimers[i].Recursive))
                            timersToRemove.Add(allTimers[i]);
                    }
                }

                if (timersToRemove.Count == 0)
                    return;

                for (int i = 0; i < timersToRemove.Count; i++)
                    AllTimers.Remove(timersToRemove[i]);

                timersToRemove.Clear();
            }
            catch (Exception e)
            {
                Debug.Log("Exception thrown in timer.");
                Debug.LogException(e);

                allTimers.RemoveAt(currentIndex);
            }
        }

        /// <summary>
        /// Removes a timer instantly
        /// </summary>
        /// <param name="givenTimer"></param>
        public static void RemoveTimer(Timer givenTimer, bool force = false)
        {
            if (givenTimer == null)
                return;

            if (force)
            {
                Instance.AllTimers.Remove(givenTimer);
            }
            else
            {
                givenTimer.Stop();
                givenTimer.MarkForCleanUp = true;
            }
        }

        /// <summary>
        /// Called when this object is enabled.
        /// </summary>
        public void OnEnable()
        {
            //Hide the TimerManager
            hideFlags = HideFlags.HideInHierarchy;

            //Make sure there is a TimerUpdater present
            EnsureTimerUpdater();

            //Subscribe callback for scene change, this is to clean up timers
            SceneManager.activeSceneChanged -= OnSceneChanged;
            SceneManager.activeSceneChanged += OnSceneChanged;
        }

        /// <summary>
        /// This gets invoked whenever the active scene has changed
        /// </summary>
        private void OnSceneChanged(Scene scene1, Scene scene2)
        {
            ClearUnPersistentTimers();

            //Re-initialize the TimerManager,this is because for some reason when building the application, the TimerUpdater gets destroyed when loading the FIRST scene
            //This means he is spawned BEFORE the first scene is loaded and it seems DontDestroyOnLoad has no effect in that case
            //That is why Initialization should be re-attempted on EVERY scene load, since it won't do anything if there is already an updater present
            EnsureTimerUpdater();
        }

        /// <summary>
        /// Makes sure there is a TimerUpdater instance
        /// </summary>
        /// <returns>True if valid TimerUpdater is found</returns>
        private bool EnsureTimerUpdater()
        {
#if UNITY_EDITOR
            //This is to avoid spawning objects in Editor at wrong times
            if (!Application.isPlaying)
                return false;
#endif

            return TimerUpdater.Instance != null;
        }
    }

}