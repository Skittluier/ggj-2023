﻿using System;
using UnityEditor;
using UnityEngine;
using UnityEngine.Profiling;

namespace RUMBLE.Utilities.Timers
{
    public class TimerConsole : EditorWindow
    {
        //Instance of this window.
        private TimerConsole _instance;
        private TimerConsole Instance
        {
            get
            {
                if (_instance == null)
                    _instance = GetWindowInstance();

                return _instance;
            }
        }

        /// <summary>
        /// Returns the instance of this window.
        /// </summary>
        public static TimerConsole GetWindowInstance()
        {
            TimerConsole thisWindow = (TimerConsole)TimerConsole.GetWindow(typeof(TimerConsole));
            thisWindow.Init();
            return thisWindow;
        }

        /// <summary>
        /// Function to call for showing the window hue
        /// </summary>
        [MenuItem("Window/Timers")]
        public static void ShowWindow()
        {
            TimerConsole thisWindow = (TimerConsole)TimerConsole.GetWindow(typeof(TimerConsole));
            thisWindow.minSize = new Vector2(650, 45);
            thisWindow.titleContent.text = "Timers";
            thisWindow.titleContent.image = EditorGUIUtility.IconContent("TreeEditor.Trash").image;
        }

        //Icons for the current status of the timers.
        private Texture normalIcon;
        private Texture pausedIcon;
        private Texture stoppedIcon;
        private Texture normalIcon_small;
        private Texture pausedIcon_small;
        private Texture stoppedIcon_small;

        //Styles for the toolbar
        private GUIStyle toolbar;
        private GUIStyle toolbarButton;

        //Refernce to the instance of the timer manager.
        private TimerManager TimerManagerInstance;
        //Index of the current selected box.
        private int selectedBox = -1;
        //Bool to determine if this window has initialized its values.
        private bool init = false;
        //The current scroll position.
        private Vector2 scrollPos = Vector2.zero;

        //Bool to filter the type of messages that are shown in the log.
        private bool showRunning = true;
        private bool showPaused = true;
        private bool showCompleted = true;

        //Determines the base size for drawn timers.
        private const int timerBaseSize = 45;
        //Size of all labels showing delegate info.
        private Vector2 delegateLabelSize = new Vector2(500, 20);

        /// <summary>
        /// Initializes all variables.
        /// </summary>
        private void Init()
        {
            Profiler.BeginSample("Init");

            normalIcon = (Texture)EditorGUIUtility.Load("console.infoicon");
            pausedIcon = (Texture)EditorGUIUtility.Load("console.warnicon");
            stoppedIcon = (Texture)EditorGUIUtility.Load("console.erroricon");

            normalIcon_small = (Texture)EditorGUIUtility.Load("console.infoicon.sml");
            pausedIcon_small = (Texture)EditorGUIUtility.Load("console.warnicon.sml");
            stoppedIcon_small = (Texture)EditorGUIUtility.Load("console.erroricon.sml");

            selectedBox = -1;

            toolbar = new GUIStyle("toolbar");
            toolbarButton = new GUIStyle("toolbarButton");

            TimerManagerInstance = TimerManager.Instance;

            init = true;
            Profiler.EndSample();
        }

        /// <summary>
        /// Draws the window.
        /// </summary>
        public void OnGUI()
        {
            //If we havent initialized, or the TimerManager is not defined.
            if (!init)
                Init();

            //Amount of running timers.
            int runningTimers = 0;
            int pausedTimers = 0;
            int stoppedTimers = 0;

            //Only update the counts, if we have a connection with the TimeManager
            if (TimerManagerInstance != null)
            {
                runningTimers = TimerManagerInstance.RunnningTimers();
                pausedTimers = TimerManagerInstance.PausedTimers();
                stoppedTimers = TimerManagerInstance.StoppedTimers();
            }

            //Draw the toolbar
            GUILayout.BeginHorizontal(toolbar, new GUILayoutOption[0]);

            if (GUILayout.Button("Pause all", toolbarButton, new GUILayoutOption[0]))
                if (TimerManagerInstance != null)
                    for (int i = 0; i < TimerManagerInstance.AllTimers.Count; i++)
                        TimerManagerInstance.AllTimers[i].Stop();

            if (GUILayout.Button("Unpause all", toolbarButton, new GUILayoutOption[0]))
                if (TimerManagerInstance != null)
                    for (int i = 0; i < TimerManagerInstance.AllTimers.Count; i++)
                        TimerManagerInstance.AllTimers[i].Resume();

            GUILayout.FlexibleSpace();

            //Toggles to determine each timer that is currently running.
            showRunning = GUILayout.Toggle(showRunning, new GUIContent((runningTimers > 999) ? "999+" : runningTimers.ToString(), normalIcon_small), toolbarButton, new GUILayoutOption[0]);
            showPaused = GUILayout.Toggle(showPaused, new GUIContent((pausedTimers > 999) ? "999+" : pausedTimers.ToString(), pausedIcon_small), toolbarButton, new GUILayoutOption[0]);
            showCompleted = GUILayout.Toggle(showCompleted, new GUIContent((stoppedTimers > 999) ? "999+" : stoppedTimers.ToString(), stoppedIcon_small), toolbarButton, new GUILayoutOption[0]);

            GUILayout.EndHorizontal();

            //Check if in playmode, else show message and stop.
            if (!EditorApplication.isPlaying)
            {
                EditorGUILayout.HelpBox("Only works in playmode", MessageType.Info);
                return;
            }

            //If there is no timermanager yet.
            if (TimerManagerInstance == null)
            {
                Init();
                EditorGUILayout.HelpBox("No TimerManager found, is the script located in your Assets folder?", MessageType.Info);
                return;
            }

            //Base offset of top toolbar.
            int totalHeight = 17;
            int scrollRectHeight = 0;

            //Based on what timers to show increment the total size of the console window.
            if (showCompleted)
                scrollRectHeight += stoppedTimers;
            if (showRunning)
                scrollRectHeight += runningTimers;
            if (showPaused)
                scrollRectHeight += pausedTimers;

            scrollRectHeight *= timerBaseSize;

            if (selectedBox != -1)
                scrollRectHeight += timerBaseSize;

            scrollPos = GUI.BeginScrollView(new Rect(0, 18, Instance.position.width, Instance.position.height), scrollPos, new Rect(0, 18, Instance.position.width - 20, scrollRectHeight));

            //Loop through all timers, and draw each of them.
            for (int i = 0; i < TimerManagerInstance.AllTimers.Count; i++)
            {
                //For each timer state check if they should be drawn.
                if (TimerManagerInstance.AllTimers[i].Paused)
                {
                    if (!showPaused)
                        continue;
                }
                else if (TimerManagerInstance.AllTimers[i].Done)
                {
                    if (!showCompleted)
                        continue;
                }
                else
                {
                    if (!showRunning)
                        continue;
                }

                //Determine the background color for the next entry.
                if ((i + 1) % 2 == 0)
                    GUI.backgroundColor = new Color32(205, 205, 205, 255);
                else
                    GUI.backgroundColor = new Color32(222, 222, 222, 255);


                //Determine if the timer should actually be drawn, only if it fits within the console box.
                bool draw = false;

                if (totalHeight - scrollPos.y < Instance.position.height)
                    draw = true;

                //Draw the timer with given info.
                totalHeight += DrawTimer(TimerManagerInstance.AllTimers[i], i, totalHeight, (selectedBox == i), draw);
            }

            //End of the scrollview.
            GUI.EndScrollView();

            //Constant repaint of this window.
            Repaint();
        }

        /// <summary>
        /// Function for drawing a timer.
        /// </summary>
        public int DrawTimer(Timer timer, int index, float yOffset, bool selected, bool draw)
        {
            //If we cannot draw this button, just return the base size.
            if (!draw)
                return timerBaseSize;

            int totalHeight = timerBaseSize;

            if (selected)
            {
                if (timer.OnTimerStarted != null)
                    totalHeight += 51;

                if (timer.OnTimerTicked != null)
                    totalHeight += 51;

                if (timer.OnTimerEnded != null)
                    totalHeight += 51;
            }

            Rect iconRect = new Rect(Instance.position.width - 45, yOffset + 5 - index, normalIcon.width, normalIcon.height);
            Rect boxRect = new Rect(0, yOffset - index, Instance.position.width, totalHeight);

            if (boxRect.Contains(Event.current.mousePosition))
            {
                if (Event.current.type == EventType.MouseDown)
                {
                    if (selectedBox == index)
                        selectedBox = -1;
                    else
                        selectedBox = index;
                }
            }

            string statusText = "Running";
            Texture statusIcon = normalIcon;

            if (timer.Done)
            {
                statusIcon = stoppedIcon;
                statusText = "Completed";
            }

            else if (timer.Paused)
            {
                statusIcon = pausedIcon;
                statusText = "Paused";
            }

            GUI.Box(boxRect, statusText);
            GUI.DrawTexture(iconRect, statusIcon);
            GUI.Label(new Rect(boxRect.x + 5, boxRect.y + 5, boxRect.width, 15), "Name = ", EditorStyles.boldLabel);
            GUI.Label(new Rect(boxRect.x + 65, boxRect.y + 5, boxRect.width, 15), "(" + timer.Name + ")", EditorStyles.miniBoldLabel);


            if (selected)
            {
                int delegateHeight = 0;

                if (timer.OnTimerStarted != null)
                    delegateHeight += DrawDelegateInfo(new Vector2(boxRect.x, boxRect.y), timer.OnTimerStarted);

                if (timer.OnTimerTicked != null)
                    delegateHeight += DrawDelegateInfo(new Vector2(boxRect.x, boxRect.y + delegateHeight), timer.OnTimerTicked);

                if (timer.OnTimerEnded != null)
                    delegateHeight += DrawDelegateInfo(new Vector2(boxRect.x, boxRect.y + delegateHeight), timer.OnTimerEnded);

                float step = Instance.position.width / 3;

                GUI.Label(new Rect(boxRect.x + step * 2, boxRect.y + 25, 100, 15), "Recursive =  ", EditorStyles.boldLabel);
                GUI.Label(new Rect(boxRect.x + step * 2, boxRect.y + 40, 100, 15), "Duration   = ", EditorStyles.boldLabel);
                GUI.Label(new Rect(boxRect.x + step * 2, boxRect.y + 55, 100, 15), "Elapsed    = ", EditorStyles.boldLabel);

                GUI.Label(new Rect(boxRect.x + step * 2 + 85, boxRect.y + 25, 100, 15), "(" + timer.Recursive + ")", EditorStyles.miniBoldLabel);
                GUI.Label(new Rect(boxRect.x + step * 2 + 85, boxRect.y + 40, 100, 15), "(" + timer.OriginalDuration + ")", EditorStyles.miniBoldLabel);
                GUI.Label(new Rect(boxRect.x + step * 2 + 85, boxRect.y + 55, 100, 15), "(" + Math.Round(timer.Elapsed, 3) + ")", EditorStyles.miniBoldLabel);
            }
            if (!selected)
            {
                float step = Instance.position.width / 3;

                GUI.Label(new Rect(boxRect.x + 5, boxRect.y + 25, 100, 20), "Recursive = ", EditorStyles.boldLabel);
                GUI.Label(new Rect(boxRect.x + 90, boxRect.y + 25, 100, 20), "(" + timer.Recursive + ")", EditorStyles.miniBoldLabel);

                GUI.Label(new Rect(boxRect.x + step, boxRect.y + 25, 100, 20), "Duration = ", EditorStyles.boldLabel);
                GUI.Label(new Rect(boxRect.x + step + 75, boxRect.y + 25, 100, 20), "(" + timer.OriginalDuration + ")", EditorStyles.miniBoldLabel);

                GUI.Label(new Rect(boxRect.x + step * 2, boxRect.y + 25, 100, 20), "Elapsed = ", EditorStyles.boldLabel);
                GUI.Label(new Rect(boxRect.x + step * 2 + 75, boxRect.y + 25, 100, 20), "(" + Math.Round(timer.Elapsed, 3) + ")", EditorStyles.miniBoldLabel);
            }


            return totalHeight;
        }

        /// <summary>
        /// Draws info about this property.
        /// </summary>
        /// <returns></returns>
        private int DrawDelegateInfo(Vector2 start, Delegate del)
        {
            int totalHeight = 0;
            int labelHeight = 17;

            //Delegate header.
            GUI.Label(new Rect(start.x + 5, start.y + 25, delegateLabelSize.x, delegateLabelSize.y), FormatTypeName(del.GetType().ToString()) + ":", EditorStyles.boldLabel);

            totalHeight += labelHeight;

            //Delegate Method.
            GUI.Label(new Rect(start.x + 55, start.y + 42, delegateLabelSize.x, delegateLabelSize.y), "Method = ", EditorStyles.boldLabel);

            if (del.Method.Name[0] == '<')
                GUI.Label(new Rect(start.x + 155, start.y + 42, delegateLabelSize.x, delegateLabelSize.y), "Custom", EditorStyles.miniBoldLabel);
            else
                GUI.Label(new Rect(start.x + 155, start.y + 42, delegateLabelSize.x, delegateLabelSize.y), del.Method.Name, EditorStyles.miniBoldLabel);

            totalHeight += labelHeight;

            //Delegate Target.
            GUI.Label(new Rect(start.x + 55, start.y + 59, delegateLabelSize.x, delegateLabelSize.y), "Target  = ", EditorStyles.boldLabel);

            if (del.Target != null)
                GUI.Label(new Rect(start.x + 155, start.y + 59, delegateLabelSize.x, delegateLabelSize.y), del.Target.ToString(), EditorStyles.miniBoldLabel);
            else
                GUI.Label(new Rect(start.x + 155, start.y + 59, delegateLabelSize.x, delegateLabelSize.y), del.Method.DeclaringType.ToString(), EditorStyles.miniBoldLabel);

            totalHeight += labelHeight;

            return totalHeight;
        }

        /// <summary>
        /// Removes the namespace from type.ToString() if there.
        /// </summary>
        private string FormatTypeName(string name)
        {
            int index = name.IndexOf('+');

            if (index == -1)
                return name;

            return name.Substring(index + 1, name.Length - (index + 1));
        }
    }
}