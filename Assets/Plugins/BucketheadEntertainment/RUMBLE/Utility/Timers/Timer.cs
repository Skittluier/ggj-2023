﻿
namespace RUMBLE.Utilities.Timers
{
    public class Timer
    {
        //Delegates
        public OnTimerStart OnTimerStarted;
        public OnTimerTick OnTimerTicked;
        public OnTimerEnd OnTimerEnded;

        //The original duration of this timer.
        public float OriginalDuration { get; private set; } = 0f;

        //The elapsed time of this timer.
        public float Elapsed { get; private set; } = 0f;

        //Bool to determine if the timer should continue or not.
        public bool Paused { get; private set; } = false;

        //Bool to represent of this timer is recursive or not.
        public bool Recursive = false;

        //Bool to determine if this Timer should be cleaned up when a new scene is loaded
        public bool IsPersistent = false;

        //Bool to determine if this timer is done.
        public bool Done => Elapsed == OriginalDuration;

        //if this is set to true, at the end of each tick the TimerManager will remove all timers with this marked true.
        public bool MarkForCleanUp = false;

        //name of the timer.
        public string Name = "New Timer";

        //Constructors.
        public Timer(float duration)
        {
            OriginalDuration = duration;
        }
        public Timer(float duration, bool recursive)
        {
            OriginalDuration = duration;
            Recursive = recursive;
        }
        public Timer(float duration, bool recursive, bool paused)
        {
            OriginalDuration = duration;
            Recursive = recursive;
            Paused = paused;
        }

        /// <summary>
        /// Ticks the timer and returns boolean value to represent if the timer has completed or not.
        /// </summary>
        /// <param name="interval">Interval to tick the timer with</param>
        public virtual bool Tick(float interval)
        {
            if (Elapsed == 0 && OnTimerStarted != null)
                OnTimerStarted.Invoke();

            if (Elapsed + interval >= OriginalDuration)
            {
                Elapsed = OriginalDuration;

                if (OnTimerTicked != null)
                    OnTimerTicked.Invoke();

                return true;
            }
            else
            {
                Elapsed += interval;

                if (OnTimerTicked != null)
                    OnTimerTicked.Invoke();

                return false;
            }
        }

        /// <summary>
        /// Sets a new duration for this timer, WARNING: will reset the timer.
        /// </summary>
        /// <param name="newDuration">The new duration of the timer.</param>
        public virtual void SetDuration(float newDuration)
        {
            OriginalDuration = newDuration;
            Reset();
        }

        /// <summary>
        /// Resets this timer.
        /// </summary>
        public virtual void Reset(bool stop = false)
        {
            if (stop)
                Stop();

            Elapsed = 0;
        }

        /// <summary>
        /// Resumes the timer (If the timer was paused.)
        /// </summary>
        public virtual void Resume()
        {
            Paused = false;
        }

        /// <summary>
        /// Stops the ticking of this timer.
        /// </summary>
        public virtual void Stop()
        {
            Paused = true;
        }

        /// <summary>
        /// Returns the current time of the timer as string format.
        /// </summary>
        /// <returns></returns>
        public virtual string FormatAsString(TimerFormat format = TimerFormat.Simple)
        {
            switch (format)
            {
                case TimerFormat.Simple:
                    return string.Format("(Duration = {0} | Elapsed = {1})", OriginalDuration, System.Math.Round(Elapsed, 2));
                case TimerFormat.Percentage:
                    return string.Format("(Percentage = {0})", System.Math.Round((Elapsed / OriginalDuration), 2));
            }

            return "";
        }

        //Delegates
        public delegate void OnTimerStart();
        public delegate void OnTimerTick();
        public delegate void OnTimerFixedTick();
        public delegate void OnTimerEnd();

        //Enum to determine the format type.
        public enum TimerFormat
        {
            Simple,
            Percentage
        }
    }
}