﻿using UnityEngine;

namespace RUMBLE.Utilities.Timers
{
    public class TimerUpdater : MonoBehaviour
    {
        private static TimerUpdater instance;
        /// <summary>
        /// Returns the current instance of the TimerUpdater, get accessor auto creates instance if not present
        /// </summary>
        public static TimerUpdater Instance
        {
            get
            {
                if (instance == null)
                    instance = FindObjectOfType<TimerUpdater>();

                if (instance == null)
                {
                    GameObject go = new GameObject("Timer Updater");
                    DontDestroyOnLoad(go);
                    go.hideFlags = HideFlags.NotEditable;
                    instance = go.AddComponent<TimerUpdater>();

                }

                return instance;
            }
            set
            {
                instance = value;
            }
        }

        /// <summary>
        /// Called each frame, used to update the TimerManager
        /// </summary>
        private void Update()
        {
            //Update the current TimerManager if its present
            TimerManager.Instance?.Update();
        }
    }
}
