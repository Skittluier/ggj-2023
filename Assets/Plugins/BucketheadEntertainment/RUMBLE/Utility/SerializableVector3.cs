﻿using UnityEngine;

namespace RUMBLE.Utilities
{
    /// <summary>
    /// An actual serializable Vector3 
    /// </summary>
    [System.Serializable]
    public class SerializableVector3
    {
        //Constructors
        public SerializableVector3() { }
        public SerializableVector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }
        public SerializableVector3(Vector3 v)
        {
            this.x = v.x;
            this.y = v.y;
            this.z = v.z;
        }

        //X coordinate
        public float x;
        //Y coordinate
        public float y;
        //Z coordinate
        public float z;

        /// <summary>
        /// Returns this as an actual vector3
        /// </summary>
        /// <returns></returns>
        public Vector3 ToVector3()
        {
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Conversion method
        /// </summary>
        public static SerializableVector3 ToSerializeable(Vector3 vector3)
        {
            return new SerializableVector3(vector3);
        }
    }
}