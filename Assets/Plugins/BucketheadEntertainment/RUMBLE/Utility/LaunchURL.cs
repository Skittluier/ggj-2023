namespace RUMBLE.Utilities
{
    using UnityEngine;

    public class LaunchURL : MonoBehaviour
    {
        [SerializeField]
        private string url;


        /// <summary>
        /// Opens a webpage.
        /// </summary>
        [ContextMenu("Open URL")]
        public void OpenURL()
        {
            Application.OpenURL(url);
        }
    }
}